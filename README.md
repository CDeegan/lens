![Lens Logo](https://i.imgur.com/N5X2XHX.png)
Lens is an Android app which combines location services and augmented reality in a purposeful way. User-generated content is given enhanced meaning through positional context. Using the Firebase platform, content is propagated across the Lens system in real-time. 

Usage
=====

 1. Sign in using a Google account
 2. Using the map, discover Lens markers in your vicinity
 3. Approach a marker and find it in augmented reality using the camera viewfinder
 4. Create and share your own markers wherever you travel

 
Requirements
============

* A Google account
* Android 5.0 or above
* GPS + Camera Permissions
* Device Gyroscope (for AR)

Motivation
============
Augmented reality is a concept that has recently become mainstream, due, largely due to the release of Pokémon Go in the summer of 2016. The CEO of Niantic, the team who developed Pokémon Go, has been quoted as stating that augmented reality technology is “interesting and promising — for technology and, really, for humanity” [(Recode Decode, 2016)](https://www.recode.net/2016/9/19/12965508/pokemon-go-john-hanke-augmented-virtual-reality-ar-recode-decode-podcast).

However, I believe that Pokémon Go is a poor example of augmented reality implementation, reduced to a gimmick that does not affect gameplay in any meaningful way. In my opinion, the context of the user’s surroundings should play a role in the implementation of augmented reality.
The goal for this project is the development of an Android app that realises this statement using a combination of location services and augmented reality.


Research
============
Location-based augmented reality is not a new topic in the realm of computer science, although discussions thus far have been confined to a relatively limited community.

The fundamental design and implementation of location-based augmented reality has been discussed by [(Geiger et al., 2013)](http://dbis.eprints.uni-ulm.de/1028/1/Geig_2014.pdf) and [(Feineis, 2013)](http://dbis.eprints.uni-ulm.de/974/1/Feineis2013(1).pdf). The findings in these papers form the foundation of this project’s augmented reality component, namely the essentials of coordinate systems and the manipulation of device rotation.
