**01/03/2017**

I did not make much progress in this project over the month of January as my time was dedicated to studying for semester one exams. In February, however, I made steady progress. As such, this blog entry will outline the past month's advancements.

## Changes to project architecture
Initially, I had planned to use a NodeJS webserver to handle Google authentication using the PassportJS middleware. In fact, I explored and implemented this solution successfully.

I developed a set of NodeJS scripts which my app interacted with to access Google's authentication functionalities. Logging in and out of Google services worked as expected and storing users' Google information in the MongoDB was relatively trivial to implement.

Unfortunately, I discovered that extending this system would be time consuming. If I planned to incorporate data storage (for user photos or videos) or a real-time database, they would have to be written from scratch. My aim for this project was to focus on the development of the app rather than a growing number of server-side scripts.

Google Firebase is the solution to these concerns. It is advertised as a mobile platform comprised of complementary features. These features include authentication, real-time databases and data storage, all of which are suitable for this project.

Firebase is architecturally represented below.

![Firebase Architecture](https://cloud.google.com/solutions/mobile/images/overview-firebase.png)

This method is a far more effective solution --  where PassportJS is a simple authentication package, Firebase is an entire backend service. It allows for Google Sign-In to be directly integrated within the app instead of routing to the Google Sign-In. It also automates real-time data synchronization across many devices -- a critical future component of the app. Firebase has extensive API documentation not only for Android, but also for iOS and Web devices. This will accelerate the development for these platforms if plans to expand the app are considered in future.

## Development

Fortunately, migrating from the old architecture to the new was completed over the course of only a weekend. This rapid integration is due to the identification of the need for an updated architecture early in the development phase. Today, the Lens app successfully uses Firebase to log in and out of Google accounts. Lens will make use of additional Firebase features in the coming months.

My next aim was to develop a simple UI for the app. The UI was to be comprised of three components: a map screen, a profile screen and a 'new post' screen. To implement this, I decided on a tab-based layout. This allows the user to swipe horizontally to navigate the aforementioned screens. The augmented reality component will be developed as a separate entity accessible from the tab-based layout.

Following this, I used the Google Maps API to develop the Lens map. Time and patience was necessary to integrate Google Maps to a standard I was satisfied with. The main complication was not with the API, but rather with the Android MapView. This view is unique to any I have worked with before in that it requires a certain degree of view lifecycle management -- the app must know how to handle the map when it is stopped or low on memory. I created a custom map style to display the map how I wanted (no place names, muted colour scheme). I also disabled access to the zoom and pan functionalities of the map. The result of this was a map that showed the entire world. The next step was to add GPS functionality.

The first challenge in this step was to handle Android permissions. What if the user denies location permission? What if they have already granted permission in previous use of the app? Next a connection to the Google API was required. On connection, the app needs to execute a location request to find the user's current geographical position. When this position changes (detected by a handler), the map updates accordingly. This was tricky to put together -- the order of each API request was of clear importance. You cannot request a location before connecting to the API and it makes little sense to detect a change in location before displaying the initial location.

I also worked on creating a logo design for the app. This logo is used in the app icon and the app login screen and will be used again in the future.

---
By the end of the month of February, all of these features had been put together and implemented in the project. This month I aim to work on the database component of the app -- uploading and reading user content.
