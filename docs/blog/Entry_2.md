## 15/11/2016 ##

For this project, I have decided to use authenticate users through Google -- a system that has been tried and tested by millions across the globe each day. In addition, this will be far quicker to implement than constructing my own authentication system from the ground up.

The project will require a web server to accommodate this user authentication system. The server will consist of a set of NodeJS scripts which will communicate with Google servers, as well as a MongoDB database which will store user data. Rather than renting a web server online, I will be using a Raspberry Pi. This small device is perfect for testing and development purposes.

The Raspberry Pi can be accessed through SSH, both inside and outside my local network. This will allow me to work on my project while away from home. A number of security steps were taken to avoid any possible vulnerabilities.

 - Limited number of login attempts
 - Login timeouts
 - Login using key authentication **ONLY**
 - Default port 22 closed, SSH traffic accepted on an alternative, hidden port.

Since my home router's external IP changes every four weeks or so, I have put a dynamic DNS in place. Simply put, I have acquired a domain name that points to the external IP. The Raspberry Pi checks for changes in the external IP at regular intervals and updates the IP the domain points to if necessary.

HTTP requests will soon be forwarded to the Pi, which will allow the Android app to communicate with it. Requests will either pertain to user authentication, which will initiate interaction with Google servers, or user data modification, which will access the Pi's database.