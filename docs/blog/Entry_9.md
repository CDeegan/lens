**27/04/2017**

## Displaying Markers

The calculation of marker coordinates was described in the previous blog post. The next goal was to display markers at these coordinates.

I decided that each marker would consist of a parent layout, an `ImageView` and a `TextView`. The `ImageView` would hold the marker icon and the `TextView` would indicate the user’s distance to the marker. I implemented this by extending a `RelativeLayout` and incorporating the two views within the class. These layouts would be displayed on another layout which overlaid the camera view. This overlay layout was updated `onSensorChanged` (very often), and to update the position of each of my custom marker layouts, each custom marker layout had to be removed from the overlay layout and drawn again at the updated position.

Initially, this worked as intended. When launching the camera view, a test marker I had set up was displayed as expected and in the correct position. Issues began to arise as I added more markers. The display began to stutter as it attempted to allocate more and more memory to the app, before eventually running out of memory. It was clear that a more efficient solution was required.

The first step in optimising these markers involved implementing a `TextView` instead of a `RelativeLayout`. Despite the name, a `TextView` allows for both text and an accompanying image to be displayed. This meant that each marker would be a single view, rather than a layout containing multiple views. This would drastically reduce the number of items on screen from three items per marker down to a single item per marker.
Removing all views and recreating them in different positions was clearly an inefficient way of updating the marker overlay. I decided to place each marker view in a HashMap when the location search retrieved a user’s post. The marker’s content was initialised at this time, but not displayed on-screen. This gave me access to a `Map` of all views I needed to be displayed. These position of these views could then be updated `onSensorChanged`, rather than removed and replaced with another view.

These actions drastically reduced the amount of memory required to display and update markers and caused the marker movements across the screen to appear much more smooth.

Now that these marker views were optimised, the appearance of each view had to be determined. Certain conditions affect how these markers are displayed:

 - Markers that are further away are scaled down to appear smaller
 - Markers that are closer are layered on top of markers that are
 - Markers outside a certain distance range appear faded as they cannot be interacted with 
 - Markers within this distance range can utilise the view’s `onTouch` implementation to display the marker’s post in a dialog.

Each of these properties are considered within the custom `TextView` implementation developed for the AR markers.

## Closing Words

I have reached the end of the development timeline for this project. There will be some minor changes made to reflect user feedback, but all of the hard work has been completed.

My next goal is to complete the documentation for this project in the form of a Technical Guide and a User Manual. I will also need to create a five minute video walkthrough. I do not foresee any complexities in these deliverables, and as such this blog will come to a close.

I have found writing in this blog to be very useful. Having my thoughts written out have helped reinforce and solidify the knowledge I have gained while developing Lens. I am sure the contents of the blog will also be very useful in my final documentation and will hopefully relieve some of the pressure over the coming weeks.