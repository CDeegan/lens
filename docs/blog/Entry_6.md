**24/03/2017**

## Code cleaning

My app functioned as I wanted it to function, however the code was not tidy enough for my liking. I have taken steps to remedy this.

Firstly, the Google API code has been removed from fragment code. It made little sense to build and initialise the API within each fragment when it could be done once at a higher level. As such, I have added a class to the app which extends the `Application` class. This class stores the global state for the entire application, regardless of which activity or fragment is active. This class initialises another new class, `GoogleAPIHelper`. `GoogleAPIHelper` is a helper class for the Google API that builds and manages the Google API connection. All Google API code is now located within a single class.

Next, permissions have been removed from the `MapFragment` class, where the app checked for location permissions. This check now occurs in the `MainActivity` class. To me, this made more sense, as the permissions pertain to all three fragments, and not just `MapFragment`. It also made it easier for me to handle the case in which the user denies permissions, in which case the user can be redirected to the login page before ever initialising the `MainActivity` components, i.e. the fragments do not need to be loaded if the user denies the required permissions.

## Location update improvements

Location updates have also been modified to enable or disable only when necessary.

The `setUserVisibleHint` method is used to determine whether location updates should be enabled or disabled. However, this method is called twice on app start. To compensate for this, a counter is used for each time the method is called. This counter simply prevents the `startLocationUpdates` method from being called twice on startup.

When changing the device’s orientation, this counter method is reset to zero (changing orientation in Android recreates the entire Fragment instance). To prevent this from happening, the counter is passed through the `OnSavedInstanceState` method, which is called when the device’s orientation changes. When the fragment is re-instantiated, the counter variable is picked up from the saved state.

## Database support for new posts

The database now stores a user’s new post. This data is stored in three locations: /posts, /users/id/posts and /geofire (detailed later). When pushing to the /posts node, a unique identifier is generated to identify the post. This ID is then used when storing the post under the user’s unique node and also the geofire node.
```json
{
  "posts" : {
    "-Kfd-2nmJ8xKdMzrkIdC" : {
      "created_by" : "HOigrdDfUWeQNxNTcnOBwua9eSo2",
      "created_on" : "23:56:30.238 19-03-2017",
      "latitude" : 53.382203,
      "longitude" : -6.4150806,
      "message" : "This is a test post"
    }
  },
  "users" : {
    "HOigrdDfUWeQNxNTcnOBwua9eSo2" : {
      "created_on" : "20:24:20.129 11-03-2017",
      "email" : "ciarandee@gmail.com",
      "last_login" : "23:55:11.366 19-03-2017",
      "photo_url" : "https://lh5.googleusercontent.com/-wHopE9QLLn0/AAAAAAAAAAI/AAAAAAAAAAA/ADPlhfLsyWqsMtBwm_iMyadEzJAp6MfEFQ/s96-c/photo.jpg",
      "posts" : {
        "-Kfd-2nmJ8xKdMzrkIdC" : {
          "created_on" : "23:56:30.238 19-03-2017",
          "latitude" : 53.382203,
          "longitude" : -6.4150806,
          "message" : "This is a test post"
        }
      },
      "username" : "Ciarán Deegan"
    }
  }
}
```
This is a necessary case of data redundancy in favour of more efficient information retrieval. The post must be within the user’s node as it allows for their posts to be retrieved for display in `ProfileFragment`’s `RecyclerView` by simply looping through all posts at the location.  However, the latitude and longitude values are too deep within the structure here for effective searching of nearby markers. This is the reason for the /posts node, where the latitude and longitude values have a depth of two rather than four.

## Realtime RecyclerView

The `RecyclerView` has been modified to handle the creation and deletion of a user’s posts as it is now linked to the Firebase Database. The view gets a handle on the database node of the user’s posts and applies a listener to it. Once data is removed or added, the view updates accordingly in real-time. Because of this, newly created posts appear on the user’s scrollable profile feed. Users can long tap on an item in this feed to remove it from the database and the feed.

When removing a post, it is removed from the three locations in which it is stored – /posts, /users/id/posts and /geofire (detailed later).

Firebase does not support ordering of retrieved results and since the post data in the database is ordered chronologically from oldest to newest, it also appeared in the `RecyclerView` chronologically, with the oldest post at the top of the feed. This issue was solved by reversing the `View` itself.

## Location Search

After much consideration of the options, I have decided to implement a location search using the GeoFire library. This library allows marker locations to be stored as a geohash rather than individual latitude and longitude values. The geohash itself is an alphanumeric `String` representation of the latitude and longitude values which has been base32-encoded. If two locations have a similar geohash, the locations must be near each other.

GeoFire stores the post’s unique key, its geohash, as well as latitude and longitude values and nothing else. This cannot be integrated into an existing node, meaning I could not merge this data with the posts node. The posts node will still be used at a later date to retrieve a posts details (user, content, date) as GeoFire does not have the ability to store additional information.

When the user launches Lens, they land on the map screen. At this point, their current location is stored. This location will be the centre point of the location search radius. GeoFire queries the database for any posts which fall within 2km of this centre point. If a post is found, a marker is placed at that location.

Once the user’s location changes, their distance from the initial centre point is calculated. If they are far enough from the centre point, the centre point is updated to their current location and another query is executed. The distance I have chosen for this is half the radius of the search area, 1km. This is visualised in this blog entry's accompanying images.

Adding a new post updates the database, which GeoFire detects as a new entry. The latitude and longitude values of this entry are used to place a new marker. This means that newly created posts are visible on the map soon after they are created. When a post is deleted, it is not yet removed from the map in real-time. The app must be restarted to clear the deleted markers from the map. This is something I aim to resolve over the weekend.

Again, orientation changes were an issue when dealing with map markers. A change in orientation cleared the map of markers. I decided to store markers returned by the GeoFire query in an `ArrayList` of type `LatLng`. This `ArrayList` is passed through the `onSavedInstanceState` method, which is executed before a change in orientation. The `ArrayList` is retrieved again in the `onCreate` method. Once the map is prepared, a marker is created at each `LatLng` location in the `ArrayList`.

---

![location_1](https://gitlab.computing.dcu.ie/deegac23/2017-ca400-deegac23/raw/master/docs/blog/images/entry-6/location_search_1.png)
*User's initial search area of 2km*

![location_2](https://gitlab.computing.dcu.ie/deegac23/2017-ca400-deegac23/raw/master/docs/blog/images/entry-6/location_search_2.png)
*User moves within 1km range, no update to search area*

![location_3](https://gitlab.computing.dcu.ie/deegac23/2017-ca400-deegac23/raw/master/docs/blog/images/entry-6/location_search_3.png)
*User moves outside 1km range, search area is updated*