**15/04/2017**

## Testing

With the due date looming ever closer, I have decided to begin structured testing. This includes unit testing, instrumented testing, acceptance testing and cross-platform testing, as well as documentation of these tests.
The unit tests are designed to test code that I wrote myself, i.e. code that does not involve third-party libraries or any Android dependencies.

The first test covers the location search areas described in the previous blog post. The mathematical formula for finding the distance between two geographic co-ordinates is defined in the [Aviation Formulary](ftp://ftp.bartol.udel.edu/anita/amir/My_thesis/Figures4Thesis/CRC_plots/Aviation%20Formulary%20V1.46.pdf). This formula was re-created in Java code to determine if, given an initial position and an updated position, the updated position is out of range of the initial position (> 1km distance between the two points). This distance measurement was an important aspect of the app to test as it decides whether the database should be queried for new locations.

Unit tests follow the conventional `setUp()` and `tearDown()` structure, in which test variables and attributes are initialised before running a test, and destroyed upon completion. This structure allows for cleaner and more flexible test code.

To access the distance measurement method declared in `MapFragment`, the Robolectric test framework is used. This framework allows for a `Fragment` to be created and its methods accessed for testing.
Instrumented testing had previously been worked on, however the UI of the app has since been completed. The instrumented tests needed modifications to reflect that. The `MainActivity` instrumented test follows a number of steps and validates that each Android `View` is in the correct state along the way:

1.	Launch `MainActivity`<br />
a.	Verify `MapFragment` active<br />
2.	Navigate to `PostFragment`<br />
a.	Verify `PostFragment` `EditText` in view<br />
b.	Verify `PostFragment` `Button` in view<br />
3.	Create a new Lens post<br />
a.	Verify `MapFragment` active<br />
4.	Navigate to `ProfileFragment`<br />
a.	Verify `RecyclerView` in view<br />
b.	Verify previously created item in view<br />
5.	Remove newly created Lens post<br />
a.	Verify previously created item no longer in view<br />

This test performs all common UI interactions and as such it has acceptable test case coverage.
To document these tests, there seems to be no common standard used in software development. From searching online, test documentation templates vary considerably. For this reason, I will write test documentation in the manner learned in the CA267: Software Testing module.

## Google Map issues

The Android Google Map is an integral component of this app, yet there have been issues with the map relating to the device orientation.

Firstly, changing orientation re-draw the `MapView` object, however it does not redraw any markers. To fix this, visible markers need to be saved upon creation and re-added to the map on orientation change. This is more difficult than it should be, as the `Marker` object is always final. There is no way to directly access a marker once it has been drawn. For this reason, the `MarkerOptions` objects are used instead, which hold references to a Marker’s attributes. A `HashMap` of `MarkerOptions` is used to draw new `Marker` objects based upon defined attributes. My previous blog post mentioned the use of `ArrayList` to solve this issue, however using a `HashMap` allows the unique post ID of the marker to be stored.

Another, more mysterious issue was that creating a new post, changing orientation and then deleting that post, did not update the map and the marker remained when it should have been removed. After much experimentation, I figured that this erroneous state was caused by fragments other than `MapFragment` attempting to update a view contained in `MapFragment`.

To solve this, I decided to place the `GoogleMap` object (which the `MapFragment` `MapView` is tied to) and the `MarkerOptions` `HashMap` higher up in the app’s hierarchy. Placing these objects in the `MainActivity` was a way of ensuring their integrity across all fragments. It seems to have fixed a very longstanding issue. The result of this is that the issue has been resolved, which allowed for the removal of the `configChanges` attribute in the app’s manifest. 

The usage of the `configChanges` attribute is considered poor practice unless you know exactly what you’re doing. The reason for this is that it prevents Android from handling configuration changes and requires the developer to handle the changes, something which can be difficult to execute due to the complexity of the Android app lifecycle. The `configChanges` attribute also prevents different layout resources for different configurations from being used. This meant the landscape layout I designed for the `ProfileFragment` was never used when this attribute was in place.

This issue took quite some time to resolve, yet the simple fact that it allowed the removal of the `configChanges` attribute has given me more confidence in the robustness of the app orientation and lifecycle handling.

## Android Camera

For the augmented reality component of this project, a basic camera view must first be implemented. I have decided to use the `Camera` API to control camera devices, as the `Camera2` API does not support older versions of Android.

A `SurfaceView` is used to present the live camera view to the user. As such, I have developed a camera class which implements the `SurfaceHolder` `Callback` interface. This class will be used to manage information updates pertaining the the `SurfaceView`.

There are three components to the aforementioned interface:
1.	`surfaceCreated`: called once the surface has been created, i.e. when launching or resuming `ARActivity`. Camera parameters are initialised here, including camera orientation.
2.	`surfaceDestroyed`: called before a surface has been created, i.e. when leaving or pausing `ARActivity`. The Camera is deconstructed here.
3.	`surfaceChanged`: called immediately after structural changes in the surface, i.e. format or size changes. Size of preview window is changed here.

The camera must maintain cohesion through lifecycle changes. There are a number of ways to achieve parallelism; in my implementation the camera is released and deconstructed in `onPause()` and is set up again in `onResume()`.

This implementation currently works as expected, however there are some null checks I would like to implement. Android devices come in all shapes and sizes, some without cameras and some with cameras that do not support certain features. While the code works for my test devices, these null checks will need to be implemented to ensure nothing breaks in other devices.

The next step after basic camera implementation is to add another view superimposed on top of the camera preview. This View will display the augmented reality markers.