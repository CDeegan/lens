**19/04/2017**

## Augmented Reality

For the augmented reality aspect of this app, points on a map (a 2-D world) must be displayed in the camera view (a 3-D world). There are a number of steps to be taken in order to achieve the desired result.

Firstly, the **projection matrix** is generated through the camera view. This matrix is defined by the width and height of this view. The projection matrix allows objects to be drawn on screen based on this width and height. Without this, the objects would appear skewed by the unequal camera view proportions.

Secondly, a **rotation matrix** must be acquired from the device sensor. The sensor returns a rotation vector which represents the orientation of the device as a combination of an angle and an axis, in which the device has rotated through an angle θ around an axis (x, y, z). In this case, x points roughly eastwards, y points to the magnetic north of earth and z points perpendicular to the ground. This is visualised below.

![Coordinate system used by the rotation vector sensor](https://gitlab.computing.dcu.ie/deegac23/2017-ca400-deegac23/raw/master/docs/blog/images/entry-8/rotation_axis.png)

For optimal accuracy, the device sensor must be of type `TYPE_ROTATION_VECTOR`. This type of sensor requires three hardware components: an accelerometer, a magnetometer and a gyroscope. These days, most smartphones have all three of these.

Next, the projection matrix is multiplied by the rotation matrix to produce a **rotated projection matrix**. This allows objects to be drawn relative to the device screen and the device’s orientation.

The final step is to produce a camera coordinate vector, which specifies the on-screen location an object is to be drawn. To find this vector, the rotated projection matrix is multiplied by the East North Up (**ENU**) value of the physical location being mapped to the screen. Finding this ENU value is a separate procedure.

## Coordinate Conversion

The database stores the geolocation of each Lens post, which consists of a latitude and longitude value. This two-dimensional system is acceptable for maps; however, it requires conversion to the previously mentioned ENU coordinate system to be useful in augmented reality.

The first step in this conversion is to first convert the geolocation to the Earth-Centred, Earth-Fixed (**ECEF**) system. This is a 3-dimensional Cartesian coordinate system, which uses the Earth’s centre of mass as the origin point (0, 0, 0). There are drawbacks to this system. The origin point used is simply too far from most locations on Earth’s surface, making it awkward to deal with closely clustered coordinates (all coordinates are biased by the large offset to the origin). The axis of ECEF is also unintuitive from the surface of the Earth.

This brings us to the next step, where the ECEF coordinates must be converted to ENU coordinates. The ENU system is a local coordinate system which fits a tangent plane to a fixed point on the surface of the Earth. It assumes a flat earth, making it a poor choice for longer distances; however, Lens only requires markers with a maximum distance of 2km to be shown, making ENU the ideal coordinate system.

![ECEF - ENU relationship](https://gitlab.computing.dcu.ie/deegac23/2017-ca400-deegac23/raw/117fbc0f9fa8220881b1a2439c87483abd554904/docs/blog/images/entry-8/ECEF_ENU_relation.png)

To implement these coordinate conversions in code, the following paper was used as a guideline:
[Converting GPS Coordinates (φλh) to Navigation
Coordinates (ENU), S. P. Drake](http://digext6.defence.gov.au/dspace/bitstream/1947/3538/1/DSTO-TN-0432.pdf)
