**16/03/2017**

## Instrumentation Tests

The Espresso testing framework provides APIs for writing UI tests that run on the device. These tests are performed using `AndroidJUnitRunner`. These UI tests are performed to ensure that all the app’s views are functioning as expected. 

The benefits of these automated tests were apparent as soon as I ran my first successful test.

- **Consistency** - The test code written is guaranteed to execute each line to completion. If the test code has thorough coverage, each element of the UI will be tested. The same cannot be said for manual UI testing – it is all too easy to forget to click a button or scroll through a list from start to finish.
- **Speed** – Automated instrumentation tests with Espresso run very quickly in comparison to manual tests. A UI task that takes 5 seconds to perform manually may take only 1 second to perform automatically.
- **Simplicity** – Espressos tests are not all that difficult to write. Once they are written, they can be run at the click of a button each time.

The most important part of writing these tests is that they leave no stone unturned. As such, I have made a note to develop these Instrumentation as I implement UI features. For example, when adding a new ScrollView, I would then write an Espresso test to verify that the view has not broken any other UI components.

Espresso tests follow a simple workflow, where a UI action is performed on a given view, and some test success condition is checked.
```
onView(withId(R.id.my_view))            // withId(R.id.my_view) is a ViewMatcher
        .perform(click())               // click() is a ViewAction
        .check(matches(isDisplayed())); // matches(isDisplayed()) is a ViewAssertion
```
## Changes to profile data

I have added new fields in the database to signify the user’s first login data and their last login date. I’m not too worried if this data is ever used, however it is reassuring to know that it’s there if it’s ever needed.

On login, the user’s profile data is stored in a `SharedPreferences` XML file. This file contains key-value pairs that can be written to or read from with ease. Data can be retrieved quickly from this local file later instead of querying the database for it. 

## RecyclerView


The `RecyclerView` is a scrollable view containing a non-fixed number of child views. In this instance, the child views are `CardViews`. The `RecyclerView` is used to display a user’s past Lens posts in a scrollable format using an adapter. This adapter binds a set of data (user posts) to the `RecyclerView`.

This same effect can be achieved using a `ListView`, which I used for last year’s project. However, the `RecyclerView`, which was developed as a direct improvement to the `ListView`, is a more efficient solution. The main advantage of the `RecyclerView` is that it recycles views. That is, views are created and replaced on the fly as the user scrolls. This means less views overall which leads to better performance. Whether a user has ten posts or a hundred posts, the performance will not significantly degrade.

## Image Retrieval

Each `CardView` within the `RecyclerView` has a location image. This image is a map snapshot of the location of the post and are retrieved from Google. Unfortunately, due to the recycling nature of the RecyclerView, these images were downloaded repeatedly as the user scrolled and views were replaced.

I was using a simple `ASyncTask` to retrieve and apply these images to a view, however these images were never cached. This caused the issue mentioned previously. The solution I decided on was to use an external library to handle images – Picasso. This library is very simple to use and handles image downloading AND caching. With this in place, cached images no longer need to be downloaded multiple times. 

Picasso also allows the setting of fall-back images in case the image fails to download as well as placeholder images to display before the actual image has been retrieved – two very handy features I had not considered in my own implementation of image downloading.

## Geolocation Search

I have been considering my options when it comes to searching the database for markers within the user’s vicinity.

The most obvious option is to use a naïve search by selecting locations with latitude and longitude values within a certain mathematical range of the user. For example, if a user is at (40,50), search and select all markers within the grid (30,60) – (50,70). Values in any of the grid’s corners will be selected, although these values are technically outside the search range of 10 units. These corner values will lead to inefficiencies.

Suzanne, my supervisor, suggested using a library called FLANN (Fast Library for Approximate Nearest Neighbors) to tackle this problem, although we agreed this might not be viable either. If the closest marker is 50km away, it will be selected as a nearest neighbour to the user’s current location.

Another option I briefly looked into is the use of a location search library, GeoFire. It allows the storing of locations with string keys. Keys within a geographic area can then be queried in realtime. This library uses Firebase to store these keys and as such it may integrate nicely into my project.

## The Soft Keyboard
Managing the soft keyboard (virtual keyboard) in Android is quite difficult. Unfortunately, it is not as simple as `Keyboard.show()` or `Keyboard.hide()`. Quite the opposite, the API for the soft keyboard is abysmal.

To close the keyboard, `InputMethodManager` must be used. To access `InputMethodManager`, a `Context` must be used. `InputMethodManager` then requires that you specify which view you want to hide the keyboard from. The above requirements lead to complications:

 - Since a `Context` must be used, all keyboard management must occur within a Fragment or Activity. To develop a utility class for the keyboard, the context must be passed into it.

 - Android views are dynamic. This is especially the case when using fragments. Swiping from tab to tab nullifies views left, right and centre. Attempting to close the keyboard on navigation from a fragment is not directly possible as its view objects will be destroyed at that point. 
 This means they keyboard should be managed from somewhere higher up in the Android View hierarchy, such as the Activity that hosts the fragments.   The code I used to manage the closing of a keyboard within `PostFragment` is present in `MainActivity` (the parent Activity class).   This made it difficult to debug issues in which the keyboard   displayed undesired behaviour.

