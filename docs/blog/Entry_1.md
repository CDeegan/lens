## 28/10/2016 ##

This blog will outline the development process of my final year project, Lens. The following is a description of the project and what it entails.


----------


**Project Title** 

Lens 

**General area covered by the project** 

Location-based services, Augmented Reality 

**Description of the proposed project** 

The proposed project is the development of a platform for a new way of sharing ideas, thoughts and comments. The platform will consist of an Android app, a webserver and a database system. The app will allow users to create and submit text-based messages which are bound to the location in which they were submitted from. These messages can then be viewed in the real world at their respective locations using a combination of the camera of the user’s device and augmented reality. 

**Background** 

During my INTRA internship I worked on an augmented reality Android app. Once the project was complete, I decided I wanted to continue developing with augmented reality technologies. At the time, coincidentally, Pokémon Go was released as a “location-based augmented reality game”. While I could see the appeal of the app, I do not think it realised the true potential of combining location-based services and augmented reality. I began brainstorming ideas which tied the two technologies together and eventually arrived on the underlying concept of my current project. 

**Purpose** 

The proposed platform will tap into the previously mentioned potential of combining location-based services and augmented reality. It will provide a unique way of sharing content since the recipients of any content published will be anyone who can physically reach the content’s location. Discovering new content does not involve scrolling a news feed or tapping a notification, instead the user must physically engage with the world around them. 

**Justification**

This app will be unlike any other currently available -- it is a new and innovative concept. In addition, it would be a highly marketable product for consumers and businesses alike. The consumer could use the app to share their messages and memos, while businesses could advertise their products or services. This project also has the capability of expanding in the future with additional features which further increases its marketability. 

**Programming languages** 

Java, NodeJS 

**Learning Challenges** 

 - Document-oriented database systems  
 - Developing with location services

**Hardware / software platform** 

*Android App* 

To be developed using Android Studio and various third-party libraries.

*Linux Server* 

During development, I will use a Raspberry Pi as a server. It will consist of a NodeJS web server and a MongoDB database. 

**Special hardware / software requirements - Describe any special requirements** 

The app should be compatible with all newer Android devices. However, I predict that a device accelerometer may be a requirement.


----------
I have started the development of this project by generating a simple Android Studio project, which will act as the skeleton for the app.

As the project will require a web server, I have decided to use a Raspberry Pi to act as the server. This web server will house any necessary NodeJS scripts which will afford the Android app extended functionality, such as user authentication or accessing the MongoDB database also stored on the server.