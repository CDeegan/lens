**10/03/2017**

## Activity Navigation

Managing navigation in an Android app can be tricky due to the operating system’s Back button. To manipulate Back navigation, it is necessary to both understand the Android activity lifecycle and visualise the activities active at a given time.

In my app, using the back button would navigate to the previous activity. In `MainActivity`, the back button navigated the user to the login screen and at the log in screen, the user was navigated to the splash screen. This behaviour was undesirable.

To solve this, both `SplashActivity` and `LoginActivity` must call `finish()`, which in turn calls the Activity’s `onDestroy()` method. This means these activities are both considered finished. They are not added to the back-stack, which the Back button reads from to determine its jump-to point. The result of this is that using the Back button in `LoginActivity` and `MainActivity` now closes the app. This is the behaviour I was looking for.

To test this navigation, Android Studio’s Graphics State tool was used. This tool, used while the app is running and connected to Android Studio, creates a text file of the app’s graphical information in its current state. Within this text file is a section called View Hierarchy, which lists the activities in the back-stack.

The path tested was loading the app and logging in to view the main activity. The back stack of these paths was compared both with and without the `finish()` methods applied to `SplashActivity` and `LoginActivity`.

 - Before using the `finish()` methods:
    ```
    com.cdeegan.lens/com.cdeegan.lens.SplashActivity/android.view.ViewRootImpl@a1cb2d
    7 views, 5.52 kB of display lists
    com.cdeegan.lens/com.cdeegan.lens.LoginActivity/android.view.ViewRootImpl@2996862
    14 views, 11.05 kB of display lists
    com.cdeegan.lens/com.cdeegan.lens.MainActivity/android.view.ViewRootImpl@6eb7df3
    37 views, 36.63 kB of display lists
    ```
 - After using the `finish()` methods:
    ```
    com.cdeegan.lens/com.cdeegan.lens.MainActivity/android.view.ViewRootImpl@f588a0c
    37 views, 36.63 kB of display lists
    ```
    
## Location Request Efficiency

The activity lifecycle is also an important component of the map’s location requests. When the activity is inactive, location requests should cease. These must be resumed when the activity is made active again. These cases had to be manually catered for within `MapFragment` when I noticed `onLocationChanged()` being called while the app was running in the background.

Similarly, when the app detects that the map is not the active view (the user has swiped left or right from it), location requests can be disabled. As such, when returning to the map view, the location may not be immediately available since it takes time to re-establish a connection with the Google API. To help with this, the user’s last known location (LL) can be used while the `onLocationChanged()` listener is yet to initiate. This is the location the user was last positioned at before swiping to another view. 

This LL is also used when launching the app for the first time because the LL may still be loaded in memory. For example, launching Google Maps will store an LL which Lens can use while awaiting a connection to the Google API for an updated location. Since an LL is not always available, the case where the LL is null must have coverage.

To make these location requests even more efficient, both a timer and a displacement measure are used. This means `onLocationChanged()` is only called when the user has moved a certain distance. If the user is moving fast enough, calls to `onLocationChanged()` are throttled depending on a set interval. 

## Firebase Database

When authenticating users via Firebase, their authentication information is stored. This allows the app to re-authenticate users.

However, this stored authentication information is not readable by the app. As such, the user information required must be passed to the Firebase Database -- a separate entity to the Firebase Authentication store. It is a NoSQL database which stores data in JSON format.

This is achieved my manipulating the `FirebaseUser` object created by Firebase Authentication. This user object has several profiles, one for each login provider. In my case, each user had Firebase and Google as its provider. Had the user logged in with Facebook their listed providers would have been Firebase and Facebook. The Firebase profile is the unique profile in each case. As such, this is the profile I decided to store in my Firebase Database.

The database follows a very simple structure at the moment:
```json
{
  "users": {
    "HOigrdDfUWeQNxNTcnOBwua9eSo2": {
		"email": "test@example.com",
		"photo_url": "https://lh5.googleusercontent.com/-wHopE9QLLn0/...",
		"username": "Joe Bloggs"
    },
    "cdcFSD443OB4321fDsDWMWe9eBf2": { ... },
    "LkdncD453fAScdPDKFvfdSdc5c09": { ... }
  }
}

```
Another top-level group for the users’ markers will later be implemented. The nodes of this group will act as indexes -- as the data will be closer to the surface, this will allow for quicker searching. The user-marker relationship will also be declared under the user’s record.

---

I aim to move away from the location code over the next few days. I have a design in mind for the remaining fragments and I intend to implement more of the app's UI this weekend.
