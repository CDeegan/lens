package com.cdeegan.lens;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.opengl.Matrix;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//android.hardware.Camera deprecated, yet android.hardware.Camera2 does not support older APIs
@SuppressWarnings({"MissingPermission", "deprecation"})
public class ARActivity extends Activity implements LocationListener
{
	private final static String TAG = ARActivity.class.getSimpleName();
	private static final double DISTANCE_PER_QUERY = 0.25;
	private static final double SEARCH_RADIUS = 0.5;
	private boolean geoQueried;
	private double searchFromLat, searchFromLong;
	private Context thisContext;
	private SensorManager sensorManager;
	private SensorEventListener sensorEventListener;
	private Camera camera;
	private LensViewfinder lensViewfinder;
	private FrameLayout camContainer;
	private RelativeLayout arOverlay;
	private SurfaceView camSurface;
	private FloatingActionButton launchMainButton;
	private Button nearbyButton;
	private GridView nearbyGrid;
	private ARGridAdapter nearbyGridAdapter;
	private GlobalState state;
	private LocationRequest locationRequest;
	private GoogleApiClient googleApiClient;
	private Location currentLocation;
	private GeoQuery geoQuery;
	private List<ARMarkerView> markersNearby;
	private Map<String, ARMarkerView> arMarkers = new HashMap<>();
	private float[] rotatedProjMatrix = new float[16];

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ar);
		thisContext = getApplicationContext();

		markersNearby = new ArrayList<>();

		state = (GlobalState) thisContext.getApplicationContext();
		googleApiClient = state.getGoogleApiHelper().getGoogleApiClient();
		sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);

		launchMainButton = (FloatingActionButton) findViewById(R.id.launch_main_fab);
		nearbyButton = (Button) findViewById(R.id.ar_nearby_button);
		nearbyGrid = (GridView) findViewById(R.id.ar_nearby_grid);
		nearbyGridAdapter = new ARGridAdapter(thisContext, markersNearby);
		nearbyGrid.setAdapter(nearbyGridAdapter);

		camContainer = (FrameLayout) findViewById(R.id.cam_container_layout);
		camSurface = (SurfaceView) findViewById(R.id.cam_surface_view);
		arOverlay = new RelativeLayout(thisContext);

		nearbyButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				toggleGridVisibility();
			}
		});
		launchMainButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				startActivity(new Intent(ARActivity.this, MainActivity.class));
			}
		});

		//Define location request parameters
		locationRequest = new LocationRequest();
		locationRequest.setInterval(1000);
		locationRequest.setSmallestDisplacement(10);
		locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

		//Inform users if their device is missing the required sensors
		if(sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) == null)
		{
			final Snackbar noSensorSnackbar = Snackbar.make(findViewById(R.id.ar_activity_coord), R.string.error_sensor_missing, Snackbar.LENGTH_INDEFINITE);
			noSensorSnackbar.show();
			/* When the Snackbar is shown, the GridView is pushed up. To account for this, the top
			 * margin of the GridView must be increased by the height of the Snackbar.
			 * Values are converted from dp to px as setMargins only takes px values.
			 * Snackbar height is not known until display, which is why a callback is used.
			 */
			noSensorSnackbar.addCallback(new Snackbar.Callback()
			 {
				@Override
				public void onShown(Snackbar snackbar)
				{
					//Gridview margins must be adjusted to account for snackbar
					float density = thisContext.getResources().getDisplayMetrics().density;
					ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) nearbyGrid.getLayoutParams();

					float snackbarHeightDp = (float) noSensorSnackbar.getView().getHeight() / density;

					int marginLeft = (int)(15 * density);
					//top margin = normal margin (15dp) + snackbar height
					int marginTop = (int)((15 + snackbarHeightDp) * density);
					int marginRight = (int)(15 * density);
					int marginBottom = (int)(90 * density);
					layoutParams.setMargins(marginLeft, marginTop, marginRight, marginBottom);
				}
			 });
		}

		// used to determine if a new database query is required
		geoQueried = false;
	}

	private void startLocationUpdates()
	{
		Log.i(TAG, "startLocationUpdates");
		if(googleApiClient != null && googleApiClient.isConnected())
		{
			currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
			if(currentLocation != null)
			{
				state.setLastLocation(currentLocation);
				updateAROverlay();
			}
			LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
		}
	}

	private void stopLocationUpdates()
	{
		Log.i(TAG, "stopLocationUpdates");
		if(googleApiClient != null && googleApiClient.isConnected())
		{
			LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
		}
	}


	@Override
	public void onLocationChanged(Location location)
	{
		currentLocation = location;
		state.setLastLocation(location);
		// Check if database needs querying
		if(searchFromLat != 0 && searchFromLong != 0)
		{
			geoQueried = userWithinRange(location, searchFromLat, searchFromLong);
		}
		// Query database
		if(!geoQueried)
		{
			geoQueried = true;
			defineLocationSearch(location);
		}
		updateAROverlay();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		initialiseSensors();
		startLocationUpdates();
		initialiseViewfinder();
		//Overlay must be drawn AFTER SurfaceView setup (it is attached to the camContainer layout)
		initialiseOverlay();
		//If searched before, search again since listener was removed onPause()
		if(geoQueried)
		{
			defineLocationSearch(state.getLastLocation());
		}
	}

	@Override
	public void onPause()
	{
		super.onPause();
		stopLocationUpdates();
		unregisterSensorListeners();
		//Stop searching for markers when activity is inactive
		if(geoQueried)
		{
			geoQuery.removeAllListeners();
		}
		//destruct camera
		if(camera != null)
		{
			camera.setPreviewCallback(null);
			camera.stopPreview();
			lensViewfinder.setCamera(null);
			camera.release();
			camera = null;
		}
	}

	public void initialiseOverlay()
	{
		if (arOverlay.getParent() != null)
		{
			((ViewGroup) arOverlay.getParent()).removeView(arOverlay);
		}
		camContainer.addView(arOverlay);
	}

	public void initialiseViewfinder()
	{
		Log.i(TAG, "initialiseViewfinder");
		//Refresh surface view from parent
		if(camSurface.getParent() != null)
		{
			((ViewGroup) camSurface.getParent()).removeView(camSurface);
		}
		camContainer.addView(camSurface);

		//Initialise camera view
		if(lensViewfinder == null)
		{
			lensViewfinder = new LensViewfinder(this, camSurface);
		}
		//Refresh camera view from parent
		if(lensViewfinder.getParent() != null)
		{
			((ViewGroup) lensViewfinder.getParent()).removeView(lensViewfinder);
		}
		camContainer.addView(lensViewfinder);
		//When camera is active, display should not sleep
		lensViewfinder.setKeepScreenOn(true);

		initialiseCamera();
	}

	public void initialiseCamera()
	{
		try
		{
			camera = Camera.open();
			camera.startPreview();
			lensViewfinder.setCamera(camera);
		}
		catch(RuntimeException ex)
		{
			ex.printStackTrace();
		}
	}

	public void initialiseSensors()
	{
		Log.i(TAG, "initialiseSensors");
		sensorEventListener = new SensorEventListener()
		{
			@Override
			public void onSensorChanged(SensorEvent sensorEvent)
			{
				if(sensorEvent.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR)
				{
					if(lensViewfinder != null)
					{
						/* ROTATION MATRIX
						* Defined by device sensors (accelerometer, gyroscope and magnetometer)
						* Represents the orientation of the device (axis, angle) */
						float[] rotationMatrix = new float[16];
						SensorManager.getRotationMatrixFromVector(rotationMatrix, sensorEvent.values);
						/* PROJECTION MATRIX
						* Defined by device screen dimensions
						* Represents the camera view */
						float[] projectionMatrix = lensViewfinder.getProjectionMatrix();
						/* ROTATED PROJECTION MATRIX
						* A transformation of the projection matrix in relation to its rotation
						* Allows objects to be draw on screen relative to screen size and device
						* positioning */
						rotatedProjMatrix = new float[16];
						Matrix.multiplyMM(rotatedProjMatrix, 0, projectionMatrix, 0, rotationMatrix, 0);
						updateAROverlay();
					}
				}
			}

			@Override
			public void onAccuracyChanged(Sensor sensor, int i)
			{
				//Not required
			}
		};

		sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_FASTEST);
	}

	public void unregisterSensorListeners()
	{
		sensorManager.unregisterListener(sensorEventListener);
	}

	public void updateAROverlay()
	{
		//update position of each view
		for(Map.Entry<String, ARMarkerView> entry : arMarkers.entrySet())
		{
			ARMarkerView arMarkerView = entry.getValue();
			float[] displayCoords = getItemDisplayCoordinates(currentLocation, arMarkerView);
			if(displayCoords != null)
			{
				if(!Double.isNaN(displayCoords[0]) && ! Double.isNaN(displayCoords[1]))
				{
					//updated position according to getItemDisplayCoordinates
					arMarkerView.setX(displayCoords[0]);
					arMarkerView.setY(displayCoords[1]);
					updateCustomMarkerView(arMarkerView, arMarkerView.getMarkerLocation());
					// Markers are not added to the overlay when initially retrieved, place marker in
					// overview if it should be displayed
					if(arMarkerView.getParent() == null)
					{
						RelativeLayout.LayoutParams arMarkerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
						arOverlay.addView(arMarkerView, arMarkerParams);
					}
				}
			}
		}
	}

	public float[] getItemDisplayCoordinates(Location location, ARMarkerView arMarkerView)
	{
		Location markerLocation = arMarkerView.getMarkerLocation();
		//AR markers are "floating" 1m above the ground
		markerLocation.setAltitude(location.getAltitude());

		float[] currentLocationECEF = CoordinateUtils.geodeticToECEF(location);
		float[] itemECEF = CoordinateUtils.geodeticToECEF(markerLocation);
		float[] itemENU = CoordinateUtils.ECEFtoENU(location, currentLocationECEF, itemECEF);

		//Rotated Projection Matrix * ENU vector = camera coordinate vector
		float[] displayCoordinateVector = new float[4];
		Matrix.multiplyMV(displayCoordinateVector, 0, rotatedProjMatrix, 0, itemENU, 0);

		float xCoordDisplay = displayCoordinateVector[0];
		float yCoordDisplay = displayCoordinateVector[1];
		float zCoordDisplay = displayCoordinateVector[2];
		float translationDisplay = displayCoordinateVector[3];
		//Item is only displayed when positioned in front of the user
		if(zCoordDisplay < 0)
		{
			int screenWidth = arOverlay.getWidth();
			int screenHeight = arOverlay.getHeight();
			//Perspective projection: https://www.scratchapixel.com/lessons/3d-basic-rendering/computing-pixel-coordinates-of-3d-point
			float x = (float) (0.5 + xCoordDisplay/translationDisplay) * screenWidth;
			float y = (float) (0.5 - yCoordDisplay/translationDisplay) * screenHeight;
			return new float[]{x, y};
		}
		return null;
	}

	private void defineLocationSearch(Location location)
	{
		//Set centre of search area
		searchFromLat = location.getLatitude();
		searchFromLong = location.getLongitude();

		//Get content within 2km of centre point
		GeoFire geoFire = state.getGeoFireInstance();
		geoQuery = geoFire.queryAtLocation(new GeoLocation(location.getLatitude(), location
				.getLongitude()), SEARCH_RADIUS);

		geoQuery.addGeoQueryEventListener(new GeoQueryEventListener()
		{
			@Override
			public void onKeyEntered(String key, GeoLocation location)
			{
				Log.i(TAG, String.format("Key %s entered the search area at [%f,%f]", key, location.latitude, location.longitude));
				if(!arMarkers.containsKey(key))
				{

					Location markerLocation = new Location("");
					markerLocation.setLatitude(location.latitude);
					markerLocation.setLongitude(location.longitude);
					createCustomMarkerView(key, markerLocation);
				}
			}

			@Override
			public void onKeyExited(String key)
			{
				Log.i(TAG, String.format("Key %s is no longer in the search area", key));
				//Remove marker from nearby list
				if(markersNearby.remove(arMarkers.get(key)))
				{
					Log.i(TAG, key  + " removed from nearby");
					//If no markers are nearby, hide the nearby button
					if(markersNearby.size() == 0)
					{
						nearbyButton.setVisibility(View.INVISIBLE);
						//If GridView open and no markers nearby, hide grid
						if(nearbyGrid.getVisibility() == View.VISIBLE)
						{
							toggleGridVisibility();
						}
					}
				}
				//Remove marker from view
				arOverlay.removeView(arMarkers.get(key));
				//Remove marker from map
				arMarkers.remove(key);
				nearbyGridAdapter.notifyDataSetChanged();
			}

			@Override
			public void onKeyMoved(String key, GeoLocation location)
			{
				Log.i(TAG, String.format("Key %s moved within the search area to [%f,%f]", key, location.latitude, location.longitude));
			}

			@Override
			public void onGeoQueryReady()
			{
				Log.i(TAG, "All initial data has been loaded and events have been fired!");
			}

			@Override
			public void onGeoQueryError(DatabaseError error)
			{
				Log.i(TAG, "There was an error with this query: " + error);
			}
		});
	}

	public void createCustomMarkerView(String key, Location markerLocation)
	{
		//Create AR marker view
		ARMarkerView arMarkerView = new ARMarkerView(thisContext);
		arMarkerView.setTag(key);
		arMarkerView.setKey(key);
		//AR marker appearance is based on distance from user
		arMarkerView.setMarkerLocation(currentLocation, markerLocation);
		arMarkerView.scaleView();
		arMarkerView.initialiseMarkerContent(ARActivity.this);
		arMarkers.put(key, arMarkerView);
		if(arMarkerView.isInRange())
		{
			markersNearby.add(arMarkerView);
			nearbyButton.setVisibility(View.VISIBLE);
			Log.i(TAG, arMarkerView.getKey()  + " found nearby");
			nearbyGridAdapter.notifyDataSetChanged();
		}
	}

	public void updateCustomMarkerView(ARMarkerView arMarkerView, Location markerLocation)
	{
		//AR marker appearance is based on distance from user
		arMarkerView.setMarkerLocation(currentLocation, markerLocation);
		arMarkerView.scaleView();
	}

	protected boolean userWithinRange(Location location, double latitude, double longitude)
	{
		Location initialLocation = new Location("");
		initialLocation.setLatitude(latitude);
		initialLocation.setLongitude(longitude);
		double distanceBetweenPoints = initialLocation.distanceTo(location) / 1000;
		//GeoFire searches a 2km radius -- if user moves 1km, search again
		if(distanceBetweenPoints > DISTANCE_PER_QUERY)
		{
			return false;
		}
		return true;
	}

	public void toggleGridVisibility()
	{
		if(nearbyGrid.getVisibility() == View.VISIBLE)
		{
			nearbyGrid.setVisibility(View.INVISIBLE);
			nearbyButton.setText("Nearby");
		}
		else
		{
			nearbyGrid.setVisibility(View.VISIBLE);
			nearbyButton.setText("Close");
		}
	}
}
