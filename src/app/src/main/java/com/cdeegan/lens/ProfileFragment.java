package com.cdeegan.lens;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class ProfileFragment extends Fragment
{
	//Fragment instance variables
	private Context thisContext;
	private RecyclerView profileFeed;
	private RecyclerView.Adapter feedAdapter;
	private LinearLayoutManager feedViewManager;
	private FirebaseDatabase database;
	private static final String TAG = ProfileFragment.class.getSimpleName();

	public static ProfileFragment newInstance()
	{
		return new ProfileFragment();
	}

	//Anything that doesn't involve the View hierarchy (i.e. non-graphical initialisations) is done here
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		thisContext = getContext();
		database = FirebaseDatabase.getInstance();
	}

	//Assign View variables and any graphical initialisations
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_profile, container, false);
		//Get user views
		TextView usernameText = (TextView) view.findViewById(R.id.profile_username_placeholder);
		TextView emailText = (TextView) view.findViewById(R.id.profile_email_placeholder);
		Button signOutButton = (Button) view.findViewById(R.id.profile_sign_out_button);
		profileFeed = (RecyclerView) view.findViewById(R.id.profile_feed);
		//setSelected is required for TextView marquee effect to function
		emailText.setSelected(true);
		//Set user views
		PreferencesHelper prefs = new PreferencesHelper(thisContext);
		Picasso.with(thisContext)
				.load(prefs.getStringPreference("profile_photo_url"))
				.error(R.drawable.lens_default_photo)
				.placeholder(R.drawable.lens_default_photo)
				.into((ImageView) view.findViewById(R.id.profile_photo));
		usernameText.setText(prefs.getStringPreference("profile_username"));
		emailText.setText(prefs.getStringPreference("profile_email"));

		//RecyclerView setup
		//Feed contents are of constant dimensions, resize of RecyclerView not expected
		profileFeed.setHasFixedSize(true);
		//LinearLayout manager
		feedViewManager = new LinearLayoutManager(thisContext);
		//Show data in reverse (new posts at top)
		feedViewManager.setStackFromEnd(true);
		feedViewManager.setReverseLayout(true);
		profileFeed.setLayoutManager(feedViewManager);

		//Reference to user's posts
		DatabaseReference usersPostsRef = database.getReference("users/" + prefs.getStringPreference("profile_uid") + "/posts");
		//specify adapter that will fetch updates from database
		feedAdapter = new ProfileFeedAdapter(LensPost.class, R.layout.post_feed_item, LensPostViewHolder.class, usersPostsRef);
		//Scroll RecyclerView when new data is added
		feedAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver()
		{
			@Override
			public void onItemRangeInserted(int positionStart, int itemCount)
			{
				super.onItemRangeInserted(positionStart, itemCount);
				int postCount = feedAdapter.getItemCount();
				int lastVisiblePosition = feedViewManager.findLastCompletelyVisibleItemPosition();

				// If the recycler view is initially being loaded or the user is at the bottom of
				// the list, scroll to the top of the list to show the newly added post.
				if(lastVisiblePosition == -1 || (positionStart >= (postCount - 1) && lastVisiblePosition == (positionStart - 1)))
				{
					profileFeed.scrollToPosition(positionStart);
				}
			}
		});
		profileFeed.setAdapter(feedAdapter);

		//Sign out button and listener
		signOutButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Log.d(TAG, "SignOutButton:clicked");
				boolean forcedSignOut = false;
				((MainActivity) getActivity()).signOut(forcedSignOut);
			}
		});
		return view;
	}
}
