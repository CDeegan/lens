package com.cdeegan.lens;

import android.content.Context;
import android.hardware.Camera;
import android.opengl.Matrix;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.List;

//android.hardware.Camera deprecated, yet android.hardware.Camera2 does not support older APIs
@SuppressWarnings("deprecation")
public class LensViewfinder extends ViewGroup implements SurfaceHolder.Callback
{
	private static final String TAG = LensViewfinder.class.getSimpleName();
	private Camera camera;
	private Camera.Size camPreviewSize;
	private List<Camera.Size> supportedPreviewSizes;
	private SurfaceHolder surfHolder;
	private SurfaceView surfView;
	private ARActivity arActivity;
	private float[] projectionMatrix = new float[16];

	public LensViewfinder(Context context, SurfaceView surface)
	{
		super(context);

		surfView = surface;
		arActivity = (ARActivity) context;
		surfHolder = surfView.getHolder();
		surfHolder.addCallback(this);
		surfHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void setCamera(Camera cam)
	{
		camera = cam;
		if(camera != null)
		{
			supportedPreviewSizes = camera.getParameters().getSupportedPreviewSizes();
			requestLayout();

			//Enable camera auto-focus if device supports it
			Camera.Parameters params = camera.getParameters();
			List<String> focusModes = params.getSupportedFocusModes();
			if(focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
			{
				params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
				camera.setParameters(params);
			}
		}
	}

	/**
	 * Gets a projection matrix based on the camera preview size. This matrix is later used in
	 * combination with a rotation matrix to draw markers on screen.
	 */
	public void createProjectionMatrix()
	{
		float ratio = (float) camPreviewSize.width / camPreviewSize.height;
		//parameter values from
		//https://developer.android.com/training/graphics/opengl/projection.html#projection
		Matrix.frustumM(projectionMatrix, 0, -ratio, ratio, -1, 1, 3, 7);
	}

	public float[] getProjectionMatrix()
	{
		return projectionMatrix;
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
		final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
		setMeasuredDimension(width, height);

		if (supportedPreviewSizes != null)
		{
			camPreviewSize = getOptimalPreviewSize(supportedPreviewSizes, width, height);
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder surfaceHolder)
	{
		try
		{
			/*surfaceCreated gets called before camera initialisation on app resume, check if null
			before continuing */
			if(camera != null)
			{
				//Get device orientation, apply rotation to camera
				int rotation = getCameraRotation();
				camera.setDisplayOrientation(rotation);
				camera.getParameters().setRotation(rotation);

				//set surfaceHolder as live camera view
				camera.setPreviewDisplay(surfaceHolder);
			}
		}
		catch(IOException e)
		{
			Log.d(TAG, "Camera error on surfaceCreated " + e.getMessage());
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height)
	{
		if(camera != null)
		{
			//Stop preview before making changes
			camera.stopPreview();

			/*not all devices support arbitrary preview sizes, cannot rely on parameter variables
			to set preview size*/
			Camera.Parameters parameters = camera.getParameters();
			parameters.setPreviewSize(camPreviewSize.width, camPreviewSize.height);

			//Triggers onMeasure and onLayout
			requestLayout();

			camera.setParameters(parameters);
			camera.startPreview();

			createProjectionMatrix();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder surfaceHolder)
	{
		if(camera != null)
		{
			camera.setPreviewCallback(null);
			camera.stopPreview();
			camera.release();
			camera = null;
		}
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom)
	{
		if(changed && getChildCount() > 0)
		{
			final View child = getChildAt(0);

			final int width = right - left;
			final int height = bottom - top;

			int previewWidth = width;
			int previewHeight = height;
			if(camPreviewSize != null)
			{
				previewWidth = camPreviewSize.width;
				previewHeight = camPreviewSize.height;
			}

			//Source: Professional Android Open Accessory Programming with Arduino, p273
			//Centre the child surface view within the parent.
			if(width * previewHeight > height * previewWidth)
			{
				final int scaledChildWidth = previewWidth * height / previewHeight;
				child.layout((width - scaledChildWidth) / 2, 0, (width + scaledChildWidth) / 2, height);
			}
			else
			{
				final int scaledChildHeight = previewHeight * width / previewWidth;
				child.layout(0, (height - scaledChildHeight) / 2, width, (height + scaledChildHeight) / 2);
			}
		}
	}

	//Modified from Android docs: https://developer.android.com/reference/android/hardware/Camera.html#setDisplayOrientation%28int%29
	private int getCameraRotation()
	{
		final Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);

		int rotation = arActivity.getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;

		switch(rotation)
		{
			case Surface.ROTATION_0:
				degrees = 0;
				break;
			case Surface.ROTATION_90:
				degrees = 90;
				break;
			case Surface.ROTATION_180:
				degrees = 180;
				break;
			case Surface.ROTATION_270:
				degrees = 270;
				break;
		}

		int result;
		if(info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
		{
			//forward-facing
			result = (info.orientation + degrees) % 360;
			//compensate mirror
			result = (360 - result) % 360;
		}
		else
		{
			// back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		return result;
	}

	private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h)
	{
		final double ASPECT_TOLERANCE = 0.1;
		double targetRatio = (double) h / w;

		if(sizes == null)
		{
			return null;
		}

		Camera.Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		for(Camera.Size size : sizes)
		{
			double ratio = (double) size.width / size.height;
			if(Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
			{
				continue;
			}
			if(Math.abs(size.height - h) < minDiff)
			{
				optimalSize = size;
				minDiff = Math.abs(size.height - h);
			}
		}

		if(optimalSize == null)
		{
			minDiff = Double.MAX_VALUE;
			for(Camera.Size size : sizes)
			{
				if(Math.abs(size.height - h) < minDiff)
				{
					optimalSize = size;
					minDiff = Math.abs(size.height - h);
				}
			}
		}
		return optimalSize;
	}
}
