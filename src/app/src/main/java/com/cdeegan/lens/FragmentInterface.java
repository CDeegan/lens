package com.cdeegan.lens;

public interface FragmentInterface
{
	void fragmentBecameVisible();
}
