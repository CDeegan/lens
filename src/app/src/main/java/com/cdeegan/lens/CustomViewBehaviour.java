package com.cdeegan.lens;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar.SnackbarLayout;
import android.util.AttributeSet;
import android.view.View;

/**
 * A custom view behaviour class to enable a view to move dynamically within a CoordinatorLayout,
 * e.g. when displaying a Snackbar
 */
public class CustomViewBehaviour extends CoordinatorLayout.Behavior<View>
{

	public CustomViewBehaviour(Context context, AttributeSet attrs)
	{
	}

	@Override
	public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency)
	{
		return dependency instanceof SnackbarLayout;
	}

	@Override
	public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency)
	{
		float translationY = Math.min(0, dependency.getTranslationY() - dependency.getHeight());
		child.setTranslationY(translationY);
		return true;
	}
}