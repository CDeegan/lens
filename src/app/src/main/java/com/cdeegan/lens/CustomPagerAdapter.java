package com.cdeegan.lens;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class CustomPagerAdapter extends FragmentPagerAdapter
{
	private static final int PAGE_COUNT = 3;
	private Context appContext;

	public CustomPagerAdapter(FragmentManager fm, Context appContext)
	{
		super(fm);
		this.appContext = appContext;
	}

	@Override
	public Fragment getItem(int position)
	{
		//launch a fragment and pass arguments to it
		//Args: page number, title
		switch(position)
		{
			case 0:
				return PostFragment.newInstance();
			case 1:
				return MapFragment.newInstance();
			case 2:
				return ProfileFragment.newInstance();
			default:
				return null;
		}
	}

	@Override
	public int getCount()
	{
		//This number must be equal to the number of tabs to avoid an IndexOutOfBoundsException exception
		return PAGE_COUNT;
	}

	@Override
	public CharSequence getPageTitle(int position)
	{
		//Define tab title strings
		switch(position)
		{
			case 0:
				return appContext.getResources().getString(R.string.fragment_post_title);
			case 1:
				return appContext.getResources().getString(R.string.fragment_map_title);
			case 2:
				return appContext.getResources().getString(R.string.fragment_profile_title);
			default:
				return null;
		}
	}
}
