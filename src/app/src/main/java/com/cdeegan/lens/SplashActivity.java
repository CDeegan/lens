package com.cdeegan.lens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		GlobalState state = (GlobalState) getApplication();
		state.setupGoogleApiHelper();
		state.setUpGeofire();

		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
		//Finish Activity, remove from back-stack
		finish();

		//TODO: Check if internet connection available
		//TODO: Check if Lens web server is available
		//TODO: Preload required assets
	}
}
