package com.cdeegan.lens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ARGridAdapter extends BaseAdapter
{
	private Context context;
	private List<ARMarkerView> arItems;


	public ARGridAdapter(Context ctx, List<ARMarkerView> items)
	{
		context = ctx;
		arItems = items;
	}

	@Override
	public int getCount()
	{
		return arItems.size();
	}

	@Override
	public Object getItem(int position)
	{
		return arItems.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent)
	{
		final ARMarkerView arMarkerView = arItems.get(position);
		//If view not recycled, create it
		if (convertView == null)
		{
			final LayoutInflater layoutInflater = LayoutInflater.from(context);
			convertView = layoutInflater.inflate(R.layout.nearby_grid_item, null);
		}

		//Get TextView
		final TextView itemTextView = (TextView) convertView.findViewById(R.id.nearby_grid_item_textview);

		//Set TextView text
		itemTextView.setText(arMarkerView.getText());

		//Set view click listener to show dialog
		convertView.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				arMarkerView.getMarkerDialog().show();
			}
		});

		return convertView;
	}
}