package com.cdeegan.lens;

import android.location.Location;
import android.util.Log;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

public class LensPost
{
	private static final String TAG = LensPost.class.getSimpleName();
	private String created_by, created_on, message;
	private double latitude, longitude;

	public LensPost()
	{
		//Default constructor required for calls to DataSnapshot.getValue(LensPost.class)
	}

	public LensPost(String created_by, String message, Location postLocation)
	{
		this.created_by = created_by;
		this.message = message;
		this.latitude = postLocation.getLatitude();
		this.longitude = postLocation.getLongitude();
		created_on = GlobalState.getAppDateFormat().format(new Date());
	}

	public void writePostToDB()
	{
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		GeoFire geoFire = new GeoFire(database.getReference("geofire"));
		DatabaseReference postRef = database.getReference("posts").push();
		String postID = postRef.getKey();
		DatabaseReference userRef = database.getReference("users")
				.child(created_by)
				.child("posts")
				.child(postID);

		postRef.setValue(this);
		userRef.setValue(this);

		geoFire.setLocation(postRef.getKey(), new GeoLocation(latitude, longitude), new GeoFire.CompletionListener()
		{
			@Override
			public void onComplete(String key, DatabaseError error)
			{
				if(error == null)
				{
					Log.d(TAG, "Location saved successfully");
				}
				else
				{
					Log.d(TAG, "There was an error saving the location to GeoFire: " + error);
				}
			}
		});
	}

	//Firebase requires public getters for each class variable

	public String getCreated_by()
	{
		return created_by;
	}

	public void setCreated_by(String created_by)
	{
		this.created_by = created_by;
	}

	public String getCreated_on()
	{
		return created_on;
	}

	public void setCreated_on(String created_on)
	{
		this.created_on = created_on;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public double getLatitude()
	{
		return latitude;
	}

	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	public double getLongitude()
	{
		return longitude;
	}

	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}
}
