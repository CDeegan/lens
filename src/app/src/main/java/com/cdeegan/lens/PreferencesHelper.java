package com.cdeegan.lens;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesHelper
{
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor editor;

	public PreferencesHelper(Context context)
	{
		this.sharedPreferences = context.getSharedPreferences("LensPrefs", Context.MODE_PRIVATE);
		this.editor = sharedPreferences.edit();
	}

	public String getStringPreference(String key)
	{
		return sharedPreferences.getString(key, "");
	}

	public int getIntPreference(String key)
	{
		return sharedPreferences.getInt(key, 0);
	}

	public boolean getBooleanPreference(String key)
	{
		return sharedPreferences.getBoolean(key, false);
	}

	public double getDoublePreference(String key)
	{
		return Double.longBitsToDouble(sharedPreferences.getLong(key, 0));
	}

	public void saveString(String key, String value)
	{
		editor.putString(key, value);
		editor.apply();
	}

	public void saveInt(String key, int value)
	{
		editor.putInt(key, value);
		editor.apply();
	}

	public void saveBoolean(String key, boolean value)
	{
		editor.putBoolean(key, value);
		editor.apply();
	}

	public void saveDouble(String key, double value)
	{
		editor.putLong(key, Double.doubleToRawLongBits(value));
		editor.apply();
	}

	public boolean prefExists(String key)
	{
		return sharedPreferences.contains(key);
	}

	public void clearPreference(String key)
	{
		SharedPreferences.Editor prefsEdit = sharedPreferences.edit();
		prefsEdit.remove(key);
		prefsEdit.commit();
	}

	public void clearAllPreferences()
	{
		sharedPreferences.edit().clear().commit();
	}
}
