package com.cdeegan.lens;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

class KeyboardManager
{
	static void showKeyboard(Context keyboardContext)
	{
		((InputMethodManager) (keyboardContext).getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
	}

	static void hideKeyboard(View v)
	{
		InputMethodManager imm = (InputMethodManager) v.getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
	}
}
