package com.cdeegan.lens;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.MapView;

public class PostFragment extends Fragment implements FragmentInterface
{
	//Fragment instance variables
	private GlobalState state;
	private Context thisContext;
	private static final String TAG = PostFragment.class.getSimpleName();

	private EditText editPost;
	private Button newPostButton;

	//newInstance constructor for creating fragment with arguments
	public static PostFragment newInstance()
	{
		return new PostFragment();
	}

	@Override
	public void fragmentBecameVisible()
	{
		if(editPost.requestFocus())
		{
			KeyboardManager.showKeyboard(thisContext);
		}
	}

	//Anything that doesn't involve the View hierarchy (i.e. non-graphical initialisations) is done here
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		thisContext = getContext();

		state = (GlobalState) thisContext.getApplicationContext();
	}

	//Assign View variables and any graphical initialisations
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_post, container, false);

		editPost = (EditText) view.findViewById(R.id.post_edit_text);
		newPostButton = (Button) view.findViewById(R.id.post_button_submit);
		//Handle new post submission
		newPostButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				String message = editPost.getText().toString().trim();
				if(message.length() != 0)
				{
					Location lastLocation = state.getLastLocation();

					PreferencesHelper prefs = new PreferencesHelper(thisContext);
					String creatorID = prefs.getStringPreference("profile_uid");
					LensPost newPost = new LensPost(creatorID, message, lastLocation);
					newPost.writePostToDB();

					//Get current viewPager
					MainActivity activity = (MainActivity) getActivity();

					//Get parent layout of FAB and apply to snackbar to make FAB reposition when
					//Snackbar displayed
					CoordinatorLayout coord = (CoordinatorLayout) activity.findViewById(R.id.map_fragment_coord);
					Snackbar postConfirmation = Snackbar.make(coord, R.string.fragment_post_confirmation, Snackbar.LENGTH_LONG);
					//Snackbar styling
					View snackbarView = postConfirmation.getView();
					snackbarView.setBackgroundColor(ContextCompat.getColor(thisContext, R.color.colourPrimary));
					TextView snackbarText = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
					snackbarText.setTextColor(ContextCompat.getColor(thisContext, R.color.colourWhite));
					postConfirmation.show();

					//Clear user-entered text
					editPost.getText().clear();
					//Navigate user to map fragment
					ViewPager vp = activity.getViewPager();
					vp.setCurrentItem(Tab.TAB_MAP);
				}
			}
		});
		return view;
	}
}
