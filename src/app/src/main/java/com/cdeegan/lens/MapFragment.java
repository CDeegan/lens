package com.cdeegan.lens;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseError;

import java.util.HashMap;
import java.util.Map;

//Permission is checked in MainActivity, no need to check again here
@SuppressWarnings({"MissingPermission"})
public class MapFragment extends Fragment implements LocationListener
{
	//Fragment instance variables
	private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
	private static final String TAB_VISIBLE_KEY = "TabVisibleKey";
	private static final String SEARCH_LAT_KEY = "SearchLatKey";
	private static final String SEARCH_LONG_KEY = "SearchLongKey";
	private static final String GEO_QUERIED_KEY = "GeoQueriedKey";
	private static final String TAG = MapFragment.class.getSimpleName();
	private static final int DISTANCE_PER_QUERY = 1;
	private static final int SEARCH_RADIUS = 2;
	private static final float CAMERA_ZOOM_LEVEL = 17.0f;
	private MainActivity parentActivity;
	private MapView mapView;
	private GoogleApiClient googleApiClient;
	private PreferencesHelper prefs;
	private GeoQuery geoQuery;
	private LocationRequest locationRequest;
	private GlobalState state;
	private Context thisContext;
	private HashMap<String, Marker> markers;
	private FloatingActionButton launchARButton;
	private BitmapDescriptor markerIcon;
	private boolean mapIsReady, tabIsVisible, geoQueried;
	private double searchFromLat, searchFromLong;
	private int tabVisibleCalls = 0;

	//newInstance constructor for creating fragment with arguments
	public static MapFragment newInstance()
	{
		return new MapFragment();
	}

	//Anything that doesn't involve the View hierarchy (i.e. non-graphical initialisations) is done here
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		thisContext = getContext();
		parentActivity = (MainActivity) getActivity();
		prefs = new PreferencesHelper(thisContext);

		parentActivity.setMarkerOptions(new HashMap<String, MarkerOptions>());
		markers = new HashMap<>();

		// used to determine when the map camera can move
		mapIsReady = false;
		// used to determine if a new database query is required
		geoQueried = false;
		// used to track tab state
		tabIsVisible = true;

		searchFromLat = 0;
		searchFromLong = 0;

		state = (GlobalState) thisContext.getApplicationContext();
		googleApiClient = state.getGoogleApiHelper().getGoogleApiClient();

		//Define location request in onCreate for persistence with onPause and onResume
		locationRequest = new LocationRequest();
		locationRequest.setSmallestDisplacement(10);
		locationRequest.setInterval(5000);
		locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	}

	//Assign View variables and any graphical initialisations
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_map, container, false);
		mapView = (MapView) view.findViewById(R.id.google_map);

		Bundle mapViewBundle = null;
		if(savedInstanceState != null)
		{
			// Get variables from previous state
			mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
			tabVisibleCalls = savedInstanceState.getInt(TAB_VISIBLE_KEY);
			searchFromLat = savedInstanceState.getDouble(SEARCH_LAT_KEY);
			searchFromLong = savedInstanceState.getDouble(SEARCH_LONG_KEY);
			geoQueried = savedInstanceState.getBoolean(GEO_QUERIED_KEY);
		}
		mapView.onCreate(mapViewBundle);
		setUpMap();

		launchARButton = (FloatingActionButton) view.findViewById(R.id.launch_ar_fab);
		launchARButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				startActivity(new Intent(getActivity(), ARActivity.class));
			}
		});

		return view;
	}

	/**
	 * Called twice on initial load.
	 * tabVisibleCalls used to prevent location updates from being modified on initial load
	 * (this is handled by onResume)
	 * Location updates only modified on tab change
	 *
	 * @param visible
	 */
	@Override
	public void setUserVisibleHint(boolean visible)
	{
		super.setUserVisibleHint(visible);
		tabVisibleCalls++;
		if(tabVisibleCalls > 2)
		{
			//enable or disable location updates based on fragment visibility
			if(visible)
			{
				startLocationUpdates();
				tabIsVisible = true;
			}
			else
			{
				stopLocationUpdates();
				tabIsVisible = false;
			}
		}
	}

	//LOCATION LISTENER OVERRIDE
	@Override
	public void onLocationChanged(Location location)
	{
		Log.i(TAG, "onLocationChanged");
		LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

		state.setLastLocation(location);
		//move map camera
		parentActivity.getGoogleMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, CAMERA_ZOOM_LEVEL));

		// Check if database needs querying
		if(searchFromLat != 0 && searchFromLong != 0)
		{
			geoQueried = userWithinRange(location, searchFromLat, searchFromLong);
		}
		// Query database
		if(!geoQueried)
		{
			geoQueried = true;
			defineLocationSearch(location);
		}
	}

	/**
	 * This method is used to provide an initial location before onLocationChanged gets called
	 *
	 * @param googleApiClient
	 */
	private void tryGetLastLocation(GoogleApiClient googleApiClient)
	{
		Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
		//last known location may not be available (device has not recently requested location)
		//Note: last known location can come from any app
		if(lastLocation != null)
		{
			Log.i(TAG, "getLastLocation: known");
			LatLng latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());

			state.setLastLocation(lastLocation);
			//Map is not ready when calling getLastLocation for the first time
			if(mapIsReady)
			{
				parentActivity.getGoogleMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, CAMERA_ZOOM_LEVEL));
			}
		}
		else
		{
			Log.i(TAG, "getLastLocation: not known");
		}
	}

	/**
	 * Get user's last known location before enabling location requests
	 * this provides a starting point on first run and when navigating from another fragment
	 */
	private void startLocationUpdates()
	{
		Log.i(TAG, "startLocationUpdates");
		if(googleApiClient != null && googleApiClient.isConnected())
		{
			tryGetLastLocation(googleApiClient);
			LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
		}
	}

	private void stopLocationUpdates()
	{
		Log.i(TAG, "stopLocationUpdates");
		if(googleApiClient != null && googleApiClient.isConnected())
		{
			LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
		}
	}

	protected boolean userWithinRange(Location location, double latitude, double longitude)
	{
		Location initialLocation = new Location("");
		initialLocation.setLatitude(latitude);
		initialLocation.setLongitude(longitude);
		double distanceBetweenPoints = initialLocation.distanceTo(location) / 1000;
		Log.d(TAG, "distanceBetweenPoints: " + distanceBetweenPoints);
		//GeoFire searches a 2km radius -- if user moves 1km, search again
		if(distanceBetweenPoints > DISTANCE_PER_QUERY)
		{
			return false;
		}
		return true;
	}

	private void setUpMap()
	{
		mapView.getMapAsync(new OnMapReadyCallback()
		{
			@Override
			public void onMapReady(GoogleMap mGoogleMap)
			{
				//Map Configuration
				parentActivity.setGoogleMap(mGoogleMap);
				parentActivity.getGoogleMap().setMapType(GoogleMap.MAP_TYPE_NORMAL);
				parentActivity.getGoogleMap().getUiSettings().setAllGesturesEnabled(false);
				parentActivity.getGoogleMap().getUiSettings().setMyLocationButtonEnabled(false);
				parentActivity.getGoogleMap().setMapStyle(MapStyleOptions.loadRawResourceStyle(thisContext, R.raw.custom_map_style));
				parentActivity.getGoogleMap().setMyLocationEnabled(true);
				// Disable marker click
				parentActivity.getGoogleMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
				{
					@Override
					public boolean onMarkerClick(Marker marker)
					{
						return true;
					}
				});
				if(prefs.prefExists("map_last_lat"))
				{
					//If a position is available from last run of app, place camera at that location
					//Prevents map zooming in from a great distance
					parentActivity.getGoogleMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(prefs.getDoublePreference("map_last_lat"), prefs.getDoublePreference("map_last_long")) , CAMERA_ZOOM_LEVEL));
				}

				markerIcon = BitmapDescriptorFactory.fromResource(R.drawable.lens_marker);

				mapIsReady = true;
				//This method is called on orientation change
				//if tab is not visible on orientation change, don't enable updates
				if(tabIsVisible)
				{
					startLocationUpdates();
				}
				if(parentActivity.isMarkerRedrawRequired())
				{
					//Redraw markers AFTER map has been initialised
					redrawMapMarkers();
				}
			}
		});
	}

	private void redrawMapMarkers()
	{
		//Redraw markers AFTER map has been initialised
		parentActivity.getGoogleMap().clear();
		for(Map.Entry<String, MarkerOptions> entry : parentActivity.getMarkerOptions().entrySet())
		{
			markers.put(entry.getKey(), parentActivity.getGoogleMap().addMarker(entry.getValue()));
		}
	}

	private void defineLocationSearch(Location location)
	{
		//Set centre of search area
		searchFromLat = location.getLatitude();
		searchFromLong = location.getLongitude();

		//Get content within 2km of centre point
		GeoFire geoFire = state.getGeoFireInstance();
		geoQuery = geoFire.queryAtLocation(new GeoLocation(location.getLatitude(), location
				.getLongitude()), SEARCH_RADIUS);

		geoQuery.addGeoQueryEventListener(new GeoQueryEventListener()
		{
			@Override
			public void onKeyEntered(String key, GeoLocation location)
			{
				Log.i(TAG, String.format("Key %s entered the search area at [%f,%f]", key, location.latitude, location.longitude));
				LatLng thisLocation = new LatLng(location.latitude, location.longitude);
				MarkerOptions options = new MarkerOptions().position(thisLocation).icon(markerIcon);
				//Add marker to map
				Marker queriedMarker = parentActivity.getGoogleMap().addMarker(new MarkerOptions().position(thisLocation).icon(markerIcon));
				//Add marker to markers list
				markers.put(key, queriedMarker);
				//Add marker to marker options list
				parentActivity.getMarkerOptions().put(key, options);
			}

			@Override
			public void onKeyExited(String key)
			{
				Log.i(TAG, String.format("Key %s is no longer in the search area", key));
				//Remove marker from map
				markers.get(key).remove();
				//Remove marker from markers list
				markers.remove(key);
				//Remove marker from marker options list
				parentActivity.getMarkerOptions().remove(key);
			}

			@Override
			public void onKeyMoved(String key, GeoLocation location)
			{
				Log.i(TAG, String.format("Key %s moved within the search area to [%f,%f]", key, location.latitude, location.longitude));
			}

			@Override
			public void onGeoQueryReady()
			{
				Log.i(TAG, "All initial data has been loaded and events have been fired!");
			}

			@Override
			public void onGeoQueryError(DatabaseError error)
			{
				Log.i(TAG, "There was an error with this query: " + error);
			}
		});
	}

	//LIFECYCLE METHOD OVERRIDES
	//Lifecycle methods must be forwarded to the MapView object
	@Override
	public void onResume()
	{
		super.onResume();
		mapView.onResume();
		setUpMap();
		//Get current viewPager
		MainActivity activity = (MainActivity) getActivity();
		ViewPager vp = activity.getViewPager();
		//if map active onResume and not the initial load, enable location updates
		Log.i(TAG, "onResume");
		if(vp.getCurrentItem() == Tab.TAB_MAP && mapIsReady)
		{
			startLocationUpdates();
		}
		//If searched before, search again since listener was removed onPause()
		if(geoQueried)
		{
			defineLocationSearch(state.getLastLocation());
		}
	}

	@Override
	public void onStart()
	{
		super.onStart();
		mapView.onStart();
	}

	@Override
	public void onStop()
	{
		super.onStop();
		mapView.onStop();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		mapView.onPause();
		//Get current viewPager
		MainActivity activity = (MainActivity) getActivity();
		ViewPager vp = activity.getViewPager();
		//if map active onPause, disable location updates
		if(vp.getCurrentItem() == Tab.TAB_MAP)
		{
			stopLocationUpdates();
		}
		//Stop searching for markers when activity is inactive
		if(geoQueried)
		{
			geoQuery.removeAllListeners();
		}
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		mapView.onDestroy();
	}

	@Override
	public void onLowMemory()
	{
		super.onLowMemory();
		mapView.onLowMemory();
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);

		Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
		if(mapViewBundle == null)
		{
			mapViewBundle = new Bundle();
			outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
		}
		outState.putInt(TAB_VISIBLE_KEY, tabVisibleCalls);
		outState.putDouble(SEARCH_LAT_KEY, searchFromLat);
		outState.putDouble(SEARCH_LONG_KEY, searchFromLong);
		outState.putBoolean(GEO_QUERIED_KEY, geoQueried);

		mapView.onSaveInstanceState(mapViewBundle);
	}
}
