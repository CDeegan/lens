package com.cdeegan.lens;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;

import java.util.HashMap;

public class MainActivity extends FragmentActivity
{
	private ViewPager vp;
	private PagerTabStrip pagerTabStrip;
	private CustomPagerAdapter cpAdapter;
	private int activeTab;
	private GoogleApiClient googleApiClient;
	private GoogleMap googleMap;
	private HashMap<String, MarkerOptions> markerOptions;
	private FirebaseAuth firebaseAuth;
	private Context thisContext;
	private static final String TAG = MainActivity.class.getSimpleName();
	private boolean uiReady, markerRedrawRequired;
	private static final int LOCATION_PERMISSION_REQUEST = 9997;
	private static final int CAMERA_PERMISSION_REQUEST = 9998;
	private static final int ALL_PERMISSION_REQUEST = 9999;
	private static final String MARKER_OPTIONS_KEY = "MarkerOptionsKey";
	private static final String ACTIVE_TAB_KEY = "ActiveTabKey";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		uiReady = false;
		thisContext = getApplicationContext();
		googleApiClient = GlobalState.getGoogleApiHelper().getGoogleApiClient();
		//used to determine if the map needs to recreate its markers after a state change
		markerRedrawRequired = false;

		firebaseAuth = FirebaseAuth.getInstance();

		if(savedInstanceState != null)
		{
			markerOptions = (HashMap<String, MarkerOptions>) savedInstanceState.getSerializable(MARKER_OPTIONS_KEY);
			if(markerOptions != null)
			{
				// markers will be added once map is prepared (in MapFragment)
				markerRedrawRequired = true;
			}

			//Set last active tab, used later in setupUI where ViewPager item is set
			activeTab = savedInstanceState.getInt(ACTIVE_TAB_KEY);
		}
		else
		{
			//Map tab is the initial tab, used later in setupUI where ViewPager item is set
			activeTab = Tab.TAB_MAP;
		}

		boolean locationPermissionsGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
		boolean cameraPermissionsGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;

		//Check all possible combinations of location/camera permissions to decide which permissions to request, if any
		if(!locationPermissionsGranted && !cameraPermissionsGranted)
		{
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA}, ALL_PERMISSION_REQUEST);
		}
		else if(locationPermissionsGranted && !cameraPermissionsGranted)
		{
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST);
		}
		else if(!locationPermissionsGranted && cameraPermissionsGranted)
		{
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST);
		}
		else
		{
			setupUI();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
	{
		switch(requestCode)
		{
			case CAMERA_PERMISSION_REQUEST:
			case LOCATION_PERMISSION_REQUEST:
			case ALL_PERMISSION_REQUEST:
			{
				boolean permissionsGranted = true;
				//if request cancelled, result array is empty
				if(grantResults.length > 0)
				{
					for(int grantResult : grantResults)
					{
						if(grantResult != PackageManager.PERMISSION_GRANTED)
						{
							permissionsGranted = false;
						}
					}

				}
				else
				{
					permissionsGranted = false;
				}

				if(permissionsGranted)
				{
					Log.d(TAG, "Permissions Granted");
					setupUI();
				}
				else
				{
					Log.d(TAG, "Permissions Denied");
					boolean forcedSignOut = true;
					signOut(forcedSignOut);
				}
			}
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if(uiReady)
		{
			Log.d(TAG, "onResume Active tab: " + activeTab);
			switch(activeTab)
			{
				//TODO: Double check this works
				case Tab.TAB_POST:
					KeyboardManager.showKeyboard(vp.getContext());
					break;
			}
		}
	}

	@Override
	public void onPause()
	{
		super.onPause();
		if(uiReady)
		{
			Log.d(TAG, "onPause Active tab: " + activeTab);
			switch(activeTab)
			{
				case Tab.TAB_POST:
					//Hide keyboard when app is suspended while viewing the New Post screen
					KeyboardManager.hideKeyboard(vp);
					break;
			}
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		//save all marker options and send to self on orientation change
		outState.putSerializable(MARKER_OPTIONS_KEY, markerOptions);
		outState.putInt(ACTIVE_TAB_KEY, activeTab);
	}

	private void setupUI()
	{
		vp = (ViewPager) findViewById(R.id.pager);
		pagerTabStrip = (PagerTabStrip) findViewById(R.id.pager_tab_strip);
		cpAdapter = new CustomPagerAdapter(getSupportFragmentManager(), thisContext);
		vp.setAdapter(cpAdapter);

		//Three pages, set tab to last active tab
		vp.setCurrentItem(activeTab);
		Log.d(TAG, "onCreate Active tab: " + activeTab);

		vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
		{
			@Override
			public void onPageScrolled(int tabPosition, float positionOffset, int positionOffsetPixels)
			{

			}

			@Override
			public void onPageSelected(int tabPosition)
			{
				Log.d(TAG, "onPageSelected Active tab: " + tabPosition);
				activeTab = tabPosition;
				switch(tabPosition)
				{
					case Tab.TAB_POST:
						FragmentInterface fragment = (FragmentInterface) cpAdapter.instantiateItem(vp, tabPosition);
						fragment.fragmentBecameVisible();
						break;
					default:
						//Keyboard view will be null when switching fragments, use parent view (ViewPager) to hide
						KeyboardManager.hideKeyboard(vp);
						break;
				}
			}

			@Override
			public void onPageScrollStateChanged(int state)
			{

			}
		});

		//This PagerTabStrip is not set in newer Android versions, setting manually allows PagerTabStrip to function with these versions
		//More info: https://code.google.com/p/android/issues/detail?id=213359
		((ViewPager.LayoutParams) pagerTabStrip.getLayoutParams()).isDecor = true;

		if(!isLocationEnabled(thisContext))
		{
			//Display warning, location services are disabled
			new AlertDialog.Builder(MainActivity.this).setTitle(R.string.dialog_main_location_enabled_title)
					.setMessage(R.string.dialog_main_location_enabled_message)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setPositiveButton(R.string.dialog_main_location_enabled_dismiss, new DialogInterface.OnClickListener()
					{
						public void onClick(DialogInterface dialog, int whichButton)
						{
							dialog.dismiss();
						}
					})
					.show();
		}
		uiReady = true;
	}

	public void signOut(final boolean forcedSignOut)
	{
		//Firebase sign out
		firebaseAuth.signOut();
		Log.d(TAG, "FirebaseAuth:signed_out");

		//Google sign out
		Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>()
		{
			@Override
			public void onResult(@NonNull Status status)
			{
				Log.d(TAG, "GoogleSignInApi:signed_out");
				Intent intent = new Intent(MainActivity.this, LoginActivity.class);
				//Clear back-stack
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.putExtra("forced_sign_out", forcedSignOut);
				startActivity(intent);
				//Finish Activity, remove from back-stack
				finish();
			}
		});
	}

	@Override
	public void onStop()
	{
		super.onStop();
		//save last location to file
		GlobalState state = GlobalState.getInstance();
		if(state.getLastLocation() != null)
		{
			Location lastLocation = state.getLastLocation();
			PreferencesHelper prefs = new PreferencesHelper(thisContext);
			prefs.saveDouble("map_last_lat", lastLocation.getLatitude());
			prefs.saveDouble("map_last_long", lastLocation.getLongitude());
		}
	}

	/**
	 * Method which checks if the device has location services enabled. This should be used after
	 * verifying location permissions have been granted.
	 *
	 * @param context
	 * @return true if enabled, false otherwise
	 */
	public static boolean isLocationEnabled(Context context)
	{
		int locationMode;
		try
		{
			locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
		}
		catch(Settings.SettingNotFoundException e)
		{
			e.printStackTrace();
			return false;
		}
		return locationMode != Settings.Secure.LOCATION_MODE_OFF;
	}

	/**
	 * Fragments can use this view to determine if they are active or not when a lifecycle method is
	 * called.
	 * Calling lifecycle methods within the fragment provides access to all its variables.
	 *
	 * @return the current ViewPager
	 */
	public ViewPager getViewPager()
	{
		return vp;
	}

	public GoogleMap getGoogleMap()
	{
		return googleMap;
	}

	public void setGoogleMap(GoogleMap map)
	{
		googleMap = map;
	}

	public HashMap<String, MarkerOptions> getMarkerOptions()
	{
		return markerOptions;
	}

	public void setMarkerOptions(HashMap<String, MarkerOptions> markerOptions)
	{
		this.markerOptions = markerOptions;
	}

	public boolean isMarkerRedrawRequired()
	{
		return markerRedrawRequired;
	}

	public void setMarkerRedrawRequired(boolean markerRedrawRequired)
	{
		this.markerRedrawRequired = markerRedrawRequired;
	}
}