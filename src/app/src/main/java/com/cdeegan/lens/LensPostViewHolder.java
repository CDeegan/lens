package com.cdeegan.lens;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Provide a reference to all the views for a data item in a view holder
 */
public class LensPostViewHolder extends RecyclerView.ViewHolder
{
	CardView cardView;
	ImageView postLocationIconView;
	TextView postMessageContentView;
	private LensPostViewHolder.ClickListener clickListener;

	public interface ClickListener
	{
		void onItemClick(View view, int position);
		void onItemLongClick(View view, int position);
	}

	public void setOnClickListener(LensPostViewHolder.ClickListener clickListener)
	{
		this.clickListener = clickListener;
	}

	public LensPostViewHolder(View itemView)
	{
		super(itemView);
		cardView = (CardView) itemView.findViewById(R.id.item_card);
		postLocationIconView = (ImageView) itemView.findViewById(R.id.item_post_icon);
		postMessageContentView = (TextView) itemView.findViewById(R.id.item_post_message);


		itemView.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				clickListener.onItemClick(view, getAdapterPosition());
			}
		});

		itemView.setOnLongClickListener(new View.OnLongClickListener()
		{
			@Override
			public boolean onLongClick(View view)
			{
				clickListener.onItemLongClick(view, getAdapterPosition());
				return true;
			}
		});
	}

	public void setIcon(String iconURL)
	{
		StringBuilder styleBuilder = new StringBuilder();
		//Google Maps JSON style converted to URL format
		//May be a better way to do this?
		styleBuilder.append("&style=feature:road.local|element:geometry.fill|color:0xf19f53|lightness:16|visibility:on|weight:1.3&style=feature:road.local|element:geometry.stroke|color:0xf19f53|lightness:-10&style=feature:transit|lightness:38|visibility:off");
		styleBuilder.append("&style=feature:administrative.land_parcel|visibility:off&style=feature:administrative.neighborhood|visibility:off&style=feature:landscape|color:0xf9ddc5|lightness:-7&style=feature:administrative|element:geometry|visibility:off");
		styleBuilder.append("&style=feature:road|color:0x813033|lightness:43&style=feature:road|element:labels.icon|visibility:off&style=feature:transit.station|visibility:off&style=feature:water|color:0x1994bf|saturation:-69|lightness:43|gamma:0.99");
		styleBuilder.append("&style=feature:poi.park|color:0x645c20|lightness:39&style=feature:poi.school|color:0xa95521|lightness:35&style=feature:poi.sports_complex|color:0x9e5916|lightness:32&style=feature:transit.line|color:0x813033|lightness:22");
		styleBuilder.append("&style=feature:poi|visibility:off&style=feature:poi.government|color:0x9e5916|lightness:46&style=feature:poi.medical|element:geometry.fill|color:0x813033|lightness:38|visibility:off&style=element:labels|visibility:off");
		String styles = styleBuilder.toString();

		Picasso.with(postLocationIconView.getContext())
				.load(iconURL + styles)
				.error(R.drawable.lens_default_photo)
				.placeholder(R.drawable.lens_default_photo)
				.into(postLocationIconView);
	}

	public void setContentView(String text)
	{
		postMessageContentView.setText(text);
	}
}