package com.cdeegan.lens;

import android.location.Location;

/**
 * Coordinate conversion formulas specified in "Converting GPS Coordinates(φλh) to Navigation
 * Coordinates(ENU)" (S. P. Drake)
 * Available at: http://digext6.defence.gov.au/dspace/bitstream/1947/3538/1/DSTO-TN-0432.pdf
 */
public class CoordinateUtils
{
	//length of Earth's semi-major axis (metres)
	private final static double SEMI_MAJOR_AXIS_LENGTH = 6378137.0;
	private final static double FIRST_ECCENTRICITY_SQUARED = 0.00669437999014;

	/**
	 * Convert geolocation coordinates to Earth-Centred, Earth-fixed coodinates
	 *
	 * @param location	user's current geolocation
	 * @return ECEF coordinates (x, y, z)
	 */
	public static float[] geodeticToECEF(Location location)
	{
		double latRadians = Math.toRadians(location.getLatitude());
		double longRadians = Math.toRadians(location.getLongitude());
		double altitude = location.getAltitude();

		float sinLat = (float) Math.sin(latRadians);
		float cosLat = (float) Math.cos(latRadians);
		float sinLong = (float) Math.sin(longRadians);
		float cosLong = (float) Math.cos(longRadians);
		float X = (float) Math.sqrt(1.0 - FIRST_ECCENTRICITY_SQUARED * sinLat * sinLat);

		float x = (float) (SEMI_MAJOR_AXIS_LENGTH / X + altitude) * cosLat * cosLong;
		float y = (float) (SEMI_MAJOR_AXIS_LENGTH / X + altitude) * cosLat * sinLong;
		float z = (float) ((SEMI_MAJOR_AXIS_LENGTH * (1.0 - FIRST_ECCENTRICITY_SQUARED) / X) + altitude) * sinLat;

		return new float[]{x, y, z};
	}

	/**
	 * Convert Earth-Centred, Earth-fixed coodinates to East, North, Up coordinates
	 *
	 * @param location           user's current geolocation
	 * @param userLocationECEF   user's current ECEF location
	 * @param markerLocationECEF marker's ECEF location
	 * @return East, North, Up coordinates (x, y, z)
	 */
	public static float[] ECEFtoENU(Location location, float[] userLocationECEF, float[] markerLocationECEF)
	{
		double latRadians = Math.toRadians(location.getLatitude());
		double longRadians = Math.toRadians(location.getLongitude());

		float sinLat = (float) Math.sin(latRadians);
		float cosLat = (float) Math.cos(latRadians);
		float sinLong = (float) Math.sin(longRadians);
		float cosLong = (float) Math.cos(longRadians);

		float dx = userLocationECEF[0] - markerLocationECEF[0];
		float dy = userLocationECEF[1] - markerLocationECEF[1];
		float dz = userLocationECEF[2] - markerLocationECEF[2];

		float east = (-sinLong * dx) + (cosLong * dy);
		float north = (-sinLat * cosLong * dx) + (sinLat * sinLong * dy) + (sinLat * dz);
		float up = (cosLat * cosLong * dx) + (cosLat * sinLong * dy) + (sinLat * dz);

		/* This array will later be multiplied by a 4*4 rotation matrix. As such, this array must be
		 * of length 4. A one is added to pad the array to a length of 4. */
		return new float[]{east, north, up, 1};
	}
}
