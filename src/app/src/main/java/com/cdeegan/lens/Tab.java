package com.cdeegan.lens;

/**
 * This is a helper class for the ViewPager. Each constant represents an index of the ViewPager,
 * which is 0-based. This makes it easier to identify and debug sections of code that check the
 * active tab.
 */
class Tab
{
	static final int TAB_POST = 0;
	static final int TAB_MAP = 1;
	static final int TAB_PROFILE = 2;
}
