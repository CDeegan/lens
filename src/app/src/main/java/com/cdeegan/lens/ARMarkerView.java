package com.cdeegan.lens;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ARMarkerView extends android.support.v7.widget.AppCompatTextView
{
	private static final String TAG = ARMarkerView.class.getSimpleName();
	private static final float SCALE_THRESHOLD_M = 50.0f;
	private static final int INTERACTION_THRESHOLD_M = 25;
	private Context context;
	private boolean inRange;
	private AlertDialog alertDialog;
	private Location userLocation, markerLocation;
	private String key, messageContent, creatorID, creatorUsername, dateCreated, photoURL;

	public ARMarkerView(Context context)
	{
		super(context);
		this.context = context;
		//Set View drawable (above text)
		this.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ar_marker, context
				.getTheme()), null, null);
		//Centre text beneath image
		this.setGravity(Gravity.CENTER);
	}

	public void setMarkerLocation(Location theirLocation, Location myLocation)
	{
		userLocation = theirLocation;
		markerLocation = myLocation;
	}

	public Location getMarkerLocation()
	{
		return markerLocation;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public void scaleView()
	{
		float distanceToMarker = userLocation.distanceTo(markerLocation);
		if(distanceToMarker > 1)
		{
			this.setText((int) distanceToMarker + "m");
		}
		else
		{
			this.setText("< 1m");
		}
		//Set z-index, closer items should appear on top of viewing stack (largest index at top)
		this.setZ(-distanceToMarker);
		//If marker is out of range, make view faded
		if(distanceToMarker > INTERACTION_THRESHOLD_M)
		{
			setInRange(false);
			this.setAlpha(0.25f);
			//Resize marker if far enough away
			if(distanceToMarker > SCALE_THRESHOLD_M)
			{
				//Further objects appear smaller
				float scaleFactor = SCALE_THRESHOLD_M / distanceToMarker;
				this.setScaleX(scaleFactor);
				this.setScaleY(scaleFactor);
			}
		}
		else
		{
			setInRange(true);
			this.setAlpha(1.0f);
		}
	}

	public void initialiseMarkerContent(final Activity parentActivity)
	{
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		final DatabaseReference postsRef = database.getReference("posts");
		final DatabaseReference usersRef = database.getReference("users");

		postsRef.child(key).addListenerForSingleValueEvent(new ValueEventListener()
		{
			@Override
			public void onDataChange(DataSnapshot dataSnapshot)
			{
				if(dataSnapshot.exists())
				{
					LensPost post = dataSnapshot.getValue(LensPost.class);
					messageContent = post.getMessage();
					creatorID = post.getCreated_by();
					dateCreated = post.getCreated_on();
					//This query is nested here because it depends on creatorID
					usersRef.child(creatorID)
							.addListenerForSingleValueEvent(new ValueEventListener()
							{
								@Override
								public void onDataChange(DataSnapshot dataSnapshot)
								{
									if(dataSnapshot.exists())
									{
										LensUser user = dataSnapshot.getValue(LensUser.class);
										creatorUsername = user.getUsername();
										photoURL = user.getPhoto_url();
										initialiseInteractionEvent(parentActivity);
									}
								}

								@Override
								public void onCancelled(DatabaseError databaseError)
								{
									Log.w(TAG, "onCancelled: " + databaseError.getMessage());
								}
							});
				}
			}

			@Override
			public void onCancelled(DatabaseError databaseError)
			{
				Log.w(TAG, "onCancelled: " + databaseError.getMessage());
			}
		});
	}

	public void initialiseInteractionEvent(final Activity parentActivity)
	{
		alertDialog = setUpDialog(parentActivity);
		this.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				float distanceToMarker = userLocation.distanceTo(markerLocation);
				//User can only interact with markers within a certain range
				if(distanceToMarker < INTERACTION_THRESHOLD_M)
				{
					alertDialog.show();
					String logString = "onClick:\nPost ID: " + key + "\n" + "Posted By: " + creatorUsername + " (" + creatorID + ")\n" + "Posted on: " + dateCreated + "\n" + "Content: " + messageContent;
					Log.i(TAG, logString);
				}
			}
		});
	}

	public AlertDialog setUpDialog(Activity parentActivity)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);
		// Get the layout inflater
		LayoutInflater inflater = LayoutInflater.from(context);
		View content = inflater.inflate(R.layout.dialog_ar_item, null);
		//Set dialog content dynamically
		ImageView profileImageView = (ImageView) content.findViewById(R.id.dialog_ar_profile_photo);
		TextView usernameTextView = (TextView) content.findViewById(R.id.dialog_ar_username_text);
		TextView messageTextView = (TextView) content.findViewById(R.id.dialog_ar_message_text);
		TextView dateTextView = (TextView) content.findViewById(R.id.dialog_ar_date_text);
		Picasso.with(context)
				.load(photoURL)
				.error(R.drawable.lens_default_photo)
				.placeholder(R.drawable.lens_default_photo)
				.into(profileImageView);
		usernameTextView.setText(creatorUsername);
		messageTextView.setText(messageContent);
		dateTextView.setText(dateCreated);
		// Set and show the layout for the dialog
		builder.setView(content);
		return builder.create();
	}

	public Dialog getMarkerDialog()
	{
		return alertDialog;
	}

	public boolean isInRange()
	{
		return inRange;
	}

	public void setInRange(boolean inRange)
	{
		this.inRange = inRange;
	}
}
