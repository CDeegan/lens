package com.cdeegan.lens;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.geofire.GeoFire;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class ProfileFeedAdapter extends FirebaseRecyclerAdapter<LensPost, LensPostViewHolder>
{
	private FirebaseDatabase database;
	private DatabaseReference postsRef;
	private GeoFire geoFire;
	private static final String TAG = ProfileFeedAdapter.class.getSimpleName();

	public ProfileFeedAdapter(Class<LensPost> modelClass, int modelLayout, Class<LensPostViewHolder> viewHolderClass, Query ref)
	{
		super(modelClass, modelLayout, viewHolderClass, ref);
		database = FirebaseDatabase.getInstance();
	}

	@Override
	protected void populateViewHolder(LensPostViewHolder viewHolder, LensPost post, int position)
	{
		//fit data to views of RecyclerView
		viewHolder.setIcon("https://maps.googleapis.com/maps/api/staticmap?center=" + post.getLatitude() + "," + post
				.getLongitude() + "&zoom=15&size=150x150");
		viewHolder.setContentView(post.getMessage());
	}

	@Override
	public LensPostViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		LensPostViewHolder viewHolder = super.onCreateViewHolder(parent, viewType);
		viewHolder.setOnClickListener(new LensPostViewHolder.ClickListener()
		{
			@Override
			public void onItemClick(View view, int position)
			{
				Log.d(TAG, "onItemClick " + position);
			}

			@Override
			public void onItemLongClick(final View view, final int position)
			{
				Log.d(TAG, "onItemLongClick " + position);
				//Show confirmation dialog
				new AlertDialog.Builder(view.getContext()).setTitle(R.string.fragment_profile_dialog_title)
						.setMessage(R.string.fragment_profile_dialog_message)
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setPositiveButton(R.string.fragment_profile_dialog_yes, new DialogInterface.OnClickListener()
						{
							public void onClick(DialogInterface dialog, int whichButton)
							{
								// Adapter is tied to /users/user_id/posts/. Since post is stored in
								// three locations, get reference to /posts/ and /geofire/
								final String postKey = getRef(position).getKey();
								postsRef = database.getReference("posts/" + postKey);
								geoFire = new GeoFire(database.getReference("geofire"));

								//Remove from all locations (also removes from RecyclerView)
								geoFire.removeLocation(postKey, new GeoFire.CompletionListener()
								{
									@Override
									public void onComplete(String key, DatabaseError error)
									{
										/*
										Remove post from GeoFire first. If it fails, the other
										locations should not update as it would result in data
										inconsistency
										 */
										getRef(position).removeValue();
										postsRef.removeValue();
										Log.d(TAG, "Post " + postKey + " removed");
									}
								});
							}
						})
						.setNegativeButton(R.string.fragment_profile_dialog_no, null)
						.show();
			}
		});
		return viewHolder;
	}
}
