package com.cdeegan.lens;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class LensUser
{
	private static final String TAG = LensUser.class.getSimpleName();
	private String created_on, email, last_login, photo_url, username;

	public LensUser()
	{
		//Default constructor required for calls to DataSnapshot.getValue(LensUser.class)
	}

	public LensUser(String username, String email, Uri photoUrl)
	{
		this.username = username;
		this.email = email;
		//get 400 * 400 profile picture variant
		this.photo_url = photoUrl.toString() + "?sz=400";
		this.created_on =  GlobalState.getAppDateFormat().format(new Date());
		this.last_login = this.created_on;
	}

	public void writeUserToDB(String firebase_id)
	{
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		final DatabaseReference userRef = database.getReference("users").child(firebase_id);
		final LensUser thisUser = this;

		userRef.addListenerForSingleValueEvent(new ValueEventListener()
		{
			@Override
			public void onDataChange(DataSnapshot dataSnapshot)
			{
				if(!dataSnapshot.exists())
				{
					userRef.setValue(thisUser);
				}
				else
				{
					//Update requires a String-Object Map
					Map<String, Object> taskMap = new HashMap<>();
					taskMap.put("last_login", GlobalState.getAppDateFormat().format(new Date()));
					userRef.updateChildren(taskMap);
				}
			}

			@Override
			public void onCancelled(DatabaseError databaseError)
			{
				Log.w(TAG, "onCancelled: " + databaseError.getMessage());
			}
		});
	}

	public void writeUserToPrefs(Context context, String firebase_id)
	{
		PreferencesHelper prefs = new PreferencesHelper(context);
		prefs.saveString("profile_uid", firebase_id);
		prefs.saveString("profile_username", this.username);
		prefs.saveString("profile_email", this.email);
		prefs.saveString("profile_photo_url", this.photo_url);
	}

	//Firebase requires public getters for each class variable

	public String getCreated_on()
	{
		return created_on;
	}

	public void setCreated_on(String created_on)
	{
		this.created_on = created_on;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getLast_login()
	{
		return last_login;
	}

	public void setLast_login(String last_login)
	{
		this.last_login = last_login;
	}

	public String getPhoto_url()
	{
		return photo_url;
	}

	public void setPhoto_url(String photo_url)
	{
		this.photo_url = photo_url;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}
}