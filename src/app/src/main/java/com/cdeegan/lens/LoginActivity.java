package com.cdeegan.lens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;

public class LoginActivity extends FragmentActivity
{
	private SignInButton signInButton;
	private GoogleApiClient googleApiClient;
	private FirebaseAuth firebaseAuth;
	private FirebaseAuth.AuthStateListener firebaseAuthListener;
	private Context thisContext;
	private static final int RC_SIGN_IN = 1;
	private static final String TAG = LoginActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		thisContext = getApplicationContext();

		googleApiClient = GlobalState.getGoogleApiHelper().getGoogleApiClient();
		firebaseAuth = FirebaseAuth.getInstance();

		signInButton = (SignInButton) findViewById(R.id.sign_in_button);
		signInButton.setSize(SignInButton.SIZE_STANDARD);

		signInButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				signIn();
			}
		});

		firebaseAuthListener = new FirebaseAuth.AuthStateListener()
		{
			@Override
			public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth)
			{
				FirebaseUser user = firebaseAuth.getCurrentUser();
				if(user != null)
				{
					for(UserInfo profile : user.getProviderData())
					{
						String providerId = profile.getProviderId();
						//Only interested in the user's Firebase details
						if(providerId.equals("firebase"))
						{
							LensUser lensUser = new LensUser(profile.getDisplayName(), profile.getEmail(), profile.getPhotoUrl());
							//write user to database (or update values)
							lensUser.writeUserToDB(profile.getUid());
							//write user to local file
							lensUser.writeUserToPrefs(thisContext, profile.getUid());
						}
					}
					//User is signed in, launch MainActivity
					Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
					startActivity(new Intent(LoginActivity.this, MainActivity.class));
					//Finish Activity, remove from back-stack
					finish();
				}
				else
				{
					//User is signed out
					Log.d(TAG, "onAuthStateChanged:signed_out");
				}
			}
		};

		Intent intent = getIntent();
		if(intent.hasExtra("forced_sign_out"))
		{
			if(intent.getExtras().getBoolean("forced_sign_out"))
			{
				final Snackbar permSnackbar = Snackbar.make(findViewById(R.id.activity_login), R.string.error_permissions_denied, Snackbar.LENGTH_INDEFINITE);
				permSnackbar.setAction(R.string.snackbar_login_dismiss, new View.OnClickListener()
				{
					@Override
					public void onClick(View view)
					{
						permSnackbar.dismiss();
					}
				});
				permSnackbar.show();
			}
		}
	}

	@Override
	public void onStart()
	{
		super.onStart();
		firebaseAuth.addAuthStateListener(firebaseAuthListener);
	}

	@Override
	public void onStop()
	{
		super.onStop();
		if(firebaseAuthListener != null)
		{
			firebaseAuth.removeAuthStateListener(firebaseAuthListener);
		}
	}

	private void signIn()
	{
		Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
		startActivityForResult(signInIntent, RC_SIGN_IN);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);

		// Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
		if(requestCode == RC_SIGN_IN)
		{
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
			if(result.isSuccess())
			{
				// Google Sign In was successful, authenticate with Firebase
				GoogleSignInAccount account = result.getSignInAccount();
				firebaseAuthWithGoogle(account);
			}
		}
	}

	private void firebaseAuthWithGoogle(GoogleSignInAccount acct)
	{
		Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

		AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
		firebaseAuth.signInWithCredential(credential)
				.addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
				{
					@Override
					public void onComplete(@NonNull Task<AuthResult> task)
					{
						Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

						// If sign in fails, display a message to the user. If sign in succeeds
						// the auth state listener will be notified and logic to handle the
						// signed in user can be handled in the listener.
						if(!task.isSuccessful())
						{
							Log.w(TAG, "signInWithCredential", task.getException());
							Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT)
									.show();
						}
						// ...
					}
				});
	}
}
