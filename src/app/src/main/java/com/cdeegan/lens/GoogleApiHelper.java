package com.cdeegan.lens;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

public class GoogleApiHelper implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
	private static final String TAG = GoogleApiHelper.class.getSimpleName();
	Context context;
	GoogleApiClient mGoogleApiClient;
	GoogleSignInOptions googleSignInOptions;

	public GoogleApiHelper(Context context)
	{
		this.context = context;
		buildGoogleSignInOptions();
		buildGoogleApiClient();
		connect();
	}

	public GoogleApiClient getGoogleApiClient()
	{
		return this.mGoogleApiClient;
	}

	public void connect()
	{
		if(mGoogleApiClient != null)
		{
			mGoogleApiClient.connect();
		}
	}

	public void disconnect()
	{
		if(mGoogleApiClient != null && mGoogleApiClient.isConnected())
		{
			mGoogleApiClient.disconnect();
		}
	}

	public boolean isConnected()
	{
		if(mGoogleApiClient != null)
		{
			return mGoogleApiClient.isConnected();
		}
		else
		{
			return false;
		}
	}

	private void buildGoogleSignInOptions()
	{
		googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestIdToken(context.getString(R.string.default_web_client_id))
				.requestEmail()
				.build();
	}

	private void buildGoogleApiClient()
	{
		mGoogleApiClient = new GoogleApiClient.Builder(context).addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
				.build();
	}

	@Override
	public void onConnected(Bundle bundle)
	{
		Log.d(TAG, "onConnected: googleApiClient.connect()");
	}

	@Override
	public void onConnectionSuspended(int i)
	{
		Log.d(TAG, "onConnectionSuspended: googleApiClient.connect()");
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult)
	{
		Log.d(TAG, "onConnectionFailed: connectionResult.toString() = " + connectionResult.toString());
	}
}