package com.cdeegan.lens;

import android.app.Application;
import android.location.Location;

import com.firebase.geofire.GeoFire;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class GlobalState extends Application
{
	private GoogleApiHelper googleApiHelper;
	private static GlobalState lensInstance;
	private Location lastLocation;
	private GeoFire geoFire;
	private static DateFormat appDateFormat;

	@Override
	public void onCreate()
	{
		super.onCreate();

		lensInstance = this;
		appDateFormat = new SimpleDateFormat("HH:mm:ss.SSS dd-MM-yyyy", Locale.ENGLISH);
	}

	public static synchronized GlobalState getInstance()
	{
		return lensInstance;
	}

	public GoogleApiHelper getGoogleApiHelperInstance()
	{
		return this.googleApiHelper;
	}

	public static DateFormat getAppDateFormat()
	{
		return appDateFormat;
	}

	public GeoFire getGeoFireInstance()
	{
		return geoFire;
	}

	public static GoogleApiHelper getGoogleApiHelper()
	{
		return getInstance().getGoogleApiHelperInstance();
	}

	public Location getLastLocation()
	{
		return lastLocation;
	}

	public void setLastLocation(Location lastLocation)
	{
		this.lastLocation = lastLocation;
	}

	public void setupGoogleApiHelper()
	{
		googleApiHelper = new GoogleApiHelper(lensInstance);
	}

	public void setUpGeofire()
	{
		geoFire = new GeoFire(FirebaseDatabase.getInstance().getReference("geofire"));
	}
}
