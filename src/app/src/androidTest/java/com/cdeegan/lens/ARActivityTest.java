package com.cdeegan.lens;

import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class ARActivityTest
{

	@Rule
	public ActivityTestRule<ARActivity> mActivityTestRule = new ActivityTestRule<>(ARActivity.class);

	@Test
	public void arActivityTest()
	{
		//verify FAB button present
		ViewInteraction launchMainFAB = onView(withId(R.id.launch_main_fab));
		launchMainFAB.check(matches(isDisplayed()));
		launchMainFAB.perform(click());

		//Verify FAB click launches MainActivity
		ViewInteraction mapFragment = onView(withId(R.id.map_fragment_layout));
		mapFragment.check(matches(isDisplayed()));
	}
}
