package com.cdeegan.lens;

import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.cdeegan.lens.OrientationChangeAction.orientationLandscape;
import static com.cdeegan.lens.OrientationChangeAction.orientationPortrait;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest
{

	@Rule
	public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

	@Test
	public void testMainActivityUI()
	{
		//Test in portrait orientation using swipe navigation
		activityTestWithParameters(true, true);
		//Test in portrait orientation using tap navigation
		activityTestWithParameters(false, true);
		//Test in landscape orientation using swipe navigation
		activityTestWithParameters(true, false);
		//Test in landscape orientation using tap navigation
		activityTestWithParameters(false, false);
	}

	public void activityTestWithParameters(boolean swipeMode, boolean isPortrait)
	{
		String testPostContent = "Espresso_Test_Post";
		if(isPortrait)
		{
			//Set orientation to portrait
			onView(isRoot()).perform(orientationPortrait());
		}
		else
		{
			onView(isRoot()).perform(orientationLandscape());
		}
		//Verify "Map" tab visibility
		ViewInteraction mapTab = onView(withId(R.id.map_fragment_layout));
		mapTab.check(matches(isDisplayed()));
		//Verify "New Post" title visibility
		ViewInteraction postFragmentTitle = onView(allOf(withText("New Marker"), withParent(allOf(withId(R.id.pager_tab_strip), withParent(withId(R.id.pager))))));
		postFragmentTitle.check(matches(isDisplayed()));
		if(swipeMode)
		{
			//Swipe right to navigate to "Profile" tab
			mapTab.perform(swipeRight());
		}
		else
		{
			//Click title to navigate to "New Post" tab
			postFragmentTitle.perform(click());
		}
		//Verify "New Post" tab visibility
		onView(withId(R.id.post_fragment_layout)).check(matches(isDisplayed()));

		//Verify EditText displayed and containing the correct placeholder text
		ViewInteraction newPostEditText = onView(withId(R.id.post_edit_text));
		newPostEditText.check(matches(isDisplayed()));
		newPostEditText.check(matches(withHint(R.string.fragment_post_edit_hint)));
		//Verify submit button visibility
		ViewInteraction newPostButton = onView(withId(R.id.post_button_submit));
		newPostButton.check(matches(isDisplayed()));
		//Submit test text
		newPostEditText.perform(replaceText(testPostContent), closeSoftKeyboard());
		newPostButton.perform(click());

		//Verify "Map" tab visibility (submitting a post should automatically navigate to "Map" tab)
		mapTab.check(matches(isDisplayed()));
		//Check for "Profile" title visibility
		ViewInteraction profileFragmentTitle = onView(allOf(withText("Profile"), withParent(allOf(withId(R.id.pager_tab_strip), withParent(withId(R.id.pager))))));
		profileFragmentTitle.check(matches(isDisplayed()));
		if(swipeMode)
		{
			//Swipe left to navigate to "Profile" tab
			mapTab.perform(swipeLeft());
		}
		else
		{
			//Click title to navigate to "Profile" tab
			profileFragmentTitle.perform(click());
		}
		//RecyclerView can take time to update (a few milliseconds), wait before performing checks
		pauseInstrumentation(1000);
		//Verify "Profile" tab visibility
		ViewInteraction profileTab = onView(withId(R.id.profile_fragment_layout)).check(matches(isDisplayed()));
		//Verify RecyclerView visibility
		ViewInteraction profileFeed = onView(allOf(withId(R.id.profile_feed)));
		profileFeed.check(matches(isDisplayed()));
		//Verify previously submitted text is visible in RecyclerView
		ViewInteraction profileItem = onView(allOf(withId(R.id.item_post_message), withText(testPostContent), isDisplayed()));
		profileItem.check(matches(withText(testPostContent)));
		//Delete RecyclerView item
		profileItem.perform(longClick());
		ViewInteraction confirmDeletionButton = onView(allOf(withId(android.R.id.button1), withText("Yes"), withParent(allOf(withClassName(is("com.android.internal.widget.ButtonBarLayout")), withParent(withClassName(is("android.widget.LinearLayout"))))), isDisplayed()));
		confirmDeletionButton.perform(click());
		//Item deletion is not immediate, wait 0.5 seconds
		pauseInstrumentation(500);
		//Verify RecyclerView item no longer exists
		profileItem.check(doesNotExist());
		if(swipeMode)
		{
			//Swipe left to navigate to "Map" tab
			profileTab.perform(swipeRight());
		}
		else
		{
			//Click title to navigate to "Map" tab
			ViewInteraction mapFragmentTitle = onView(allOf(withText("Map"), withParent(allOf(withId(R.id.pager_tab_strip), withParent(withId(R.id.pager))))));
			mapFragmentTitle.perform(click());
		}
	}

	/**
	 * Some UI items can take time to update, this method pauses the instrumentation for a duration
	 *
	 * @param duration the pause duration
	 */
	public void pauseInstrumentation(long duration)
	{
		try
		{
			//This is Google's recommended method of pausing during instrumentation
			Thread.sleep(duration);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}
