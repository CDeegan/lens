package com.cdeegan.lens;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/* App cannot have a currently signed-in account for this test to work, LoginActivity is bypassed by
 * logged-in users. Sign out of Lens before running this test. */
@RunWith(AndroidJUnit4.class)
public class LoginActivityTest
{

	@Rule
	public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

	@Test
	public void loginActivityTest()
	{
		//verify sign-in button present
		onView(withId(R.id.sign_in_button)).check(matches(isDisplayed()));
	}
}
