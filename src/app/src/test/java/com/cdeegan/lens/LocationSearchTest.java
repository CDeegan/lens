package com.cdeegan.lens;

import android.location.Location;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
public class LocationSearchTest
{
	private MapFragment testFragment;
	private Location initialLocation;

	@Before
	public void setUp()
	{
		testFragment = new MapFragment();

		initialLocation = new Location("");
		//Location of DCU's Larkin Lecture Theatre
		initialLocation.setLatitude(53.386198);
		initialLocation.setLongitude(-6.258163);
	}

	@Test
	public void testNewLocationOutsideRange()
	{
		//Get a location that is out of range, mathematically speaking
		Location testLocation = getOffsetLocation(initialLocation, 1010);

		boolean result = testFragment.userWithinRange(initialLocation, testLocation.getLatitude(), testLocation.getLongitude());
		assertEquals("New location within range when it should not be", result, false);
	}

	@Test
	public void testNewLocationWithinRange()
	{
		//Get a location that is within range, mathematically speaking
		Location testLocation = getOffsetLocation(initialLocation, 990);

		boolean result = testFragment.userWithinRange(initialLocation, testLocation.getLatitude(), testLocation.getLongitude());
		assertEquals("New location is not within range when it should be", result, true);
	}

	/**
	 * A method for creating a new point that is offset from an initial point by a set distance
	 * The formula is a simplified version of those found in Aviation Formulary:
	 * ftp://ftp.bartol.udel.edu/anita/amir/My_thesis/Figures4Thesis/CRC_plots/Aviation%20Formulary%20V1.46.pdf
	 * @param initialLocation	initial location offset will be measured from
	 * @param offset			distance, in metres, of new point to be created
	 * @return					the offset location
	 */
	private Location getOffsetLocation(Location initialLocation, int offset)
	{
		//Accurate to ~10m, take this into consideration when using offset
		//Get initial location
		double lat = initialLocation.getLatitude();
		double lon = initialLocation.getLongitude();

		//Earth's radius, sphere
		int earthRadius = 6378137;

		//Coordinate offsets in radians
		double dLat = offset/earthRadius;
		double dLon = offset/(earthRadius * Math.cos(Math.PI*lat/180));

		//Offset position, decimal degrees
		double latOffset = lat + dLat * 180/Math.PI;
		double lonOffset = lon + dLon * 180/Math.PI;

		//Create offsetObject
		Location offsetLocation = new Location("");
		offsetLocation.setLatitude(latOffset);
		offsetLocation.setLongitude(lonOffset);

		return offsetLocation;
	}

	@After
	public void tearDown()
	{
		testFragment = null;
		initialLocation = null;
	}
}
