package com.cdeegan.lens;

import android.location.Location;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Tests the CoordinateUtils class' coordinate conversion
 * Tests Geodetic to ECEF and ECEF to ENU
 * Tested locations scattered across the globe with good spread, varying altitudes tested at each location
 * Expected values generated using MATLAB's Aerospace functions
 */
@RunWith(RobolectricTestRunner.class)
public class CoordinateConversionTest
{
	private static final float ERROR_MARGIN = 0.001f;
	private Location testLocation;

	@Before
	public void setUp()
	{
		testLocation = new Location("");
	}

	@Test
	public void geodeticToECEFDCUSurfaceAltitude()
	{
		testLocation.setLatitude(53.385062);
		testLocation.setLongitude(-6.256787);
		testLocation.setAltitude(50);

		float expectedXValue = 3.7897f;
		float expectedYValue = -0.4155f;
		float expectedZValue = 5.0963f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFDCUPositiveAltitude()
	{
		testLocation.setLatitude(53.385062);
		testLocation.setLongitude(-6.256787);
		testLocation.setAltitude(3050);

		float expectedXValue = 3.7915f;
		float expectedYValue = -0.4157f;
		float expectedZValue = 5.0987f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFDCUNegativeAltitude()
	{
		testLocation.setLatitude(53.385062);
		testLocation.setLongitude(-6.256787);
		testLocation.setAltitude(-2950);

		float expectedXValue = 3.7879f;
		float expectedYValue = -0.4153f;
		float expectedZValue = 5.0939f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFParisSurfaceAltitude()
	{
		testLocation.setLatitude(48.856614);
		testLocation.setLongitude(2.352222);
		testLocation.setAltitude(36);

		float expectedXValue = 4.2009f;
		float expectedYValue = 0.1726f;
		float expectedZValue = 4.7801f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFParisPositiveAltitude()
	{
		testLocation.setLatitude(48.856614);
		testLocation.setLongitude(2.352222);
		testLocation.setAltitude(3036);

		float expectedXValue = 4.2029f;
		float expectedYValue = 0.1726f;
		float expectedZValue = 4.7824f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFParisNegativeAltitude()
	{
		testLocation.setLatitude(48.856614);
		testLocation.setLongitude(2.352222);
		testLocation.setAltitude(-2964);

		float expectedXValue = 4.1990f;
		float expectedYValue = 0.1725f;
		float expectedZValue = 4.7779f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFMilanSurfaceAltitude()
	{
		testLocation.setLatitude(45.465422);
		testLocation.setLongitude(9.185924);
		testLocation.setAltitude(123);

		float expectedXValue = 4.4235f;
		float expectedYValue = 0.7153f;
		float expectedZValue = 4.5239f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFMilanPositiveAltitude()
	{
		testLocation.setLatitude(45.465422);
		testLocation.setLongitude(9.185924);
		testLocation.setAltitude(3123);

		float expectedXValue = 4.4256f;
		float expectedYValue = 0.7157f;
		float expectedZValue = 4.5260f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFMilanNegativeAltitude()
	{
		testLocation.setLatitude(45.465422);
		testLocation.setLongitude(9.185924);
		testLocation.setAltitude(-2877);

		float expectedXValue = 4.4214f;
		float expectedYValue = 0.7150f;
		float expectedZValue = 4.5217f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFCairoSurfaceAltitude()
	{
		testLocation.setLatitude(30.04442);
		testLocation.setLongitude(31.235712);
		testLocation.setAltitude(26.5);

		float expectedXValue = 4.7248f;
		float expectedYValue = 2.8655f;
		float expectedZValue = 3.1747f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFCairoPositiveAltitude()
	{
		testLocation.setLatitude(30.04442);
		testLocation.setLongitude(31.235712);
		testLocation.setAltitude(3026.5);

		float expectedXValue = 4.7270f;
		float expectedYValue = 2.8668f;
		float expectedZValue = 3.1762f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFCairoNegativeAltitude()
	{
		testLocation.setLatitude(30.04442);
		testLocation.setLongitude(31.235712);
		testLocation.setAltitude(-2973.5);

		float expectedXValue = 4.7226f;
		float expectedYValue = 2.8641f;
		float expectedZValue = 3.1731f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFTorontoSurfaceAltitude()
	{
		testLocation.setLatitude(43.653226);
		testLocation.setLongitude(-79.383184);
		testLocation.setAltitude(101);

		float expectedXValue = 0.8516f;
		float expectedYValue = -4.5431f;
		float expectedZValue = 4.3804f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFTorontoPositiveAltitude()
	{
		testLocation.setLatitude(43.653226);
		testLocation.setLongitude(-79.383184);
		testLocation.setAltitude(3101);

		float expectedXValue = 0.8520f;
		float expectedYValue = -4.5452f;
		float expectedZValue = 4.3824f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFTorontoNegativeAltitude()
	{
		testLocation.setLatitude(43.653226);
		testLocation.setLongitude(-79.383184);
		testLocation.setAltitude(-2899);

		float expectedXValue = 0.8512f;
		float expectedYValue = -4.5410f;
		float expectedZValue = 4.3783f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFSydneySurfaceAltitude()
	{
		testLocation.setLatitude(-33.86882);
		testLocation.setLongitude(151.209296);
		testLocation.setAltitude(22);

		float expectedXValue = -4.6461f;
		float expectedYValue = 2.5532f;
		float expectedZValue = -3.5344f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFSydneyPositiveAltitude()
	{
		testLocation.setLatitude(-33.86882);
		testLocation.setLongitude(151.209296);
		testLocation.setAltitude(3022);

		float expectedXValue = -4.6482f;
		float expectedYValue = 2.5544f;
		float expectedZValue = -3.5361f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@Test
	public void geodeticToECEFSydneyNegativeAltitude()
	{
		testLocation.setLatitude(-33.86882);
		testLocation.setLongitude(151.209296);
		testLocation.setAltitude(-2978);

		float expectedXValue = -4.6439f;
		float expectedYValue = 2.5520f;
		float expectedZValue = -3.5327f;

		float[] ECEFCoordinates = CoordinateUtils.geodeticToECEF(testLocation);
		float actualXValue = ECEFCoordinates[0] / 1000000;
		float actualYValue = ECEFCoordinates[1] / 1000000;
		float actualZValue = ECEFCoordinates[2] / 1000000;

		assertEquals("Non-matching ECEF x-values", expectedXValue, actualXValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF y-values", expectedYValue, actualYValue, ERROR_MARGIN);
		assertEquals("Non-matching ECEF z-values", expectedZValue, actualZValue, ERROR_MARGIN);
	}

	@After
	public void tearDown()
	{
		testLocation = null;
	}
}
