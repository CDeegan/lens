package com.cdeegan.lens;

import android.location.Location;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
@Config(application = GlobalState.class)
public class LensPostTest
{
	private static final String TEST_USERNAME = "Test username";
	private static final String TEST_MSG_CONTENT = "Test content";
	private static final double TEST_LATITUDE = 0.0d;
	private static final double TEST_LONGITUDE = 1.0d;
	private Location testLocation;
	private LensPost testPost;

	@Before
	public void setUp()
	{
		testLocation = new Location("");
		testLocation.setLatitude(TEST_LATITUDE);
		testLocation.setLongitude(TEST_LONGITUDE);
		testPost = new LensPost(TEST_USERNAME, TEST_MSG_CONTENT, testLocation);
	}

	// The object must have functioning accessor methods to integrate with Firebase
	@Test
	public void postAccessorGetsValue()
	{
		assertEquals("Incorrect latitude value was accessed", 0, Double.compare(testPost.getLatitude(), TEST_LATITUDE));
		assertEquals("Incorrect longitude value was accessed", 0, Double.compare(testPost.getLongitude(), TEST_LONGITUDE));
		assertEquals("Incorrect message content value was accessed", testPost.getMessage(), TEST_MSG_CONTENT);
	}

	// The object must have functioning no-args constructor to integrate with Firebase
	@Test
	public void postNoArgsConstructor()
	{
		testPost = new LensPost();
		assertNotNull("No-args constructor failed", testPost);
	}
}

