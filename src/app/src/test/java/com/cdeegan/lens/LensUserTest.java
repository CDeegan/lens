package com.cdeegan.lens;

import android.net.Uri;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
@Config(application = GlobalState.class)
public class LensUserTest
{
	private static final String TEST_USERNAME = "Test username";
	private static final String TEST_EMAIL = "test@email.com";
	private static final String TEST_EMAIL_SUFFIX = "?sz=400";
	private static final Uri TEST_PHOTO_URI = Uri.parse("photo.test.com");
	private LensUser testUser;

	@Before
	public void setUp()
	{
		testUser = new LensUser(TEST_USERNAME, TEST_EMAIL, TEST_PHOTO_URI);
	}

	// The object must have functioning accessor methods to integrate with Firebase
	@Test
	public void userAccessorGetsValue()
	{
		assertEquals("Incorrect username value was accessed", testUser.getUsername(), TEST_USERNAME);
		assertEquals("Incorrect email value was accessed", testUser.getEmail(), TEST_EMAIL);
		assertEquals("Incorrect photo URI was accessed", testUser.getPhoto_url(), TEST_PHOTO_URI.toString() + TEST_EMAIL_SUFFIX);
	}

	// The object must have functioning no-args constructor to integrate with Firebase
	@Test
	public void userNoArgsConstructor()
	{
		testUser = new LensUser();
		assertNotNull("No-args constructor failed", testUser);
	}
}

